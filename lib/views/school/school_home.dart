import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:skuline/controllers/school_controller.dart';

class SchoolHome extends StatefulWidget {
  @override
  _SchoolHomeState createState() => _SchoolHomeState();
}

class _SchoolHomeState extends State<SchoolHome> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.green[800],
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.white, opacity: 1.0),
          title: Text("Profile Sekolah", style: TextStyle(color: Colors.white)),
          //leading: Icon(MdiIcons.accountBox, color: Colors.white),
        ),
        body: Stack(
          children: [
            Container(
              height: size.height / 8,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                color: Colors.green[800],
              ),
            ),
            GetX<SchoolController>(
                init: Get.put<SchoolController>(SchoolController()),
                builder: (SchoolController schoolController) {
                  if (schoolController != null &&
                      schoolController.schoolModel != null) {
                    return CustomScrollView(
                      slivers: [
                        SliverToBoxAdapter(
                            child: Card(
                          elevation: 3,
                          margin: EdgeInsets.all(16),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Container(
                              height: size.height - 200,
                              child: Column(children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Center(
                                    child: CircleAvatar(
                                      radius: 60,
                                      child: Icon(MdiIcons.schoolOutline,
                                          size: 60),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text(
                                  "${schoolController.schoolModel.value.sekolahName ?? " "}",
                                  style: Theme.of(context).textTheme.headline6,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 200,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(height: 20),
                                            Icon(MdiIcons.phone,
                                                color: Colors.green[800]),
                                            SizedBox(height: 20),
                                            Icon(MdiIcons.email,
                                                color: Colors.green[800]),
                                            SizedBox(height: 20),
                                            Icon(MdiIcons.web,
                                                color: Colors.green[800]),
                                            SizedBox(height: 20),
                                            Icon(MdiIcons.mapMarkerOutline,
                                                color: Colors.green[800]),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Container(
                                        height: 200,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(height: 20),
                                            Text(
                                                "${schoolController.schoolModel.value.telp ?? " "}"),
                                            SizedBox(height: 20),
                                            Text(
                                                "${schoolController.schoolModel.value.email ?? " "}"),
                                            SizedBox(height: 20),
                                            Text(
                                                "${schoolController.schoolModel.value.sekolahInfo ?? " "}"),
                                            SizedBox(height: 20),
                                            SizedBox(
                                              width: 200,
                                              child: Text("${schoolController.schoolModel.value.alamat ?? " "}, ${schoolController.schoolModel.value.villageName ?? " "}, " +
                                                  "${schoolController.schoolModel.value.subRegencyName ?? " "}, ${schoolController.schoolModel.value.regencyName ?? " "}, " +
                                                  "${schoolController.schoolModel.value.provinceName ?? " "}"),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ])),
                        )),
                      ],
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                }),
          ],
        ));
  }
}
