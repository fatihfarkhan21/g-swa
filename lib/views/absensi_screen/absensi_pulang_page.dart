import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'dart:math' show sin, cos, sqrt, atan2;
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/presensi_controller.dart';
import 'package:skuline/controllers/school_controller.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:image_picker/image_picker.dart';

class AbsensiPulang extends StatefulWidget {
  @override
  _AbsensiPulangState createState() => _AbsensiPulangState();
}

class _AbsensiPulangState extends State<AbsensiPulang> {
  var token = Get.find<AuthController>().authModel.value.token;
  PresensiController presensiController = Get.put(PresensiController());
  var platLoc = Get.find<SchoolController>().schoolModel.value.latitude;
  var plngLoc = Get.find<SchoolController>().schoolModel.value.longitude;
  final ImagePicker _picker = ImagePicker();
  File _image;

  double earthRadius = 6371000;
  double pLat = -7.315604457266959;
  double pLng = 110.16856392565859;
  String latitude, longitude;
  double total;
  StreamSubscription _locationSubscription;
  LatLng latLngPosition = LatLng(-7.242815, 109.7495733);
  //Location _locationTracker = Location();
  Marker marker;
  Circle circle;
  Position _currentPosition;
  //GoogleMapController _controller;
  GoogleMapController mapController;
  Completer<GoogleMapController> _controllerGMap = Completer();
  final _mapKey = GlobalKey<FormState>();

  static final CameraPosition initialLocation = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  Future<Uint8List> getMarker() async {
    ByteData byteData =
        await DefaultAssetBundle.of(context).load("assets/images/car_icon.png");
    return byteData.buffer.asUint8List();
  }

  void updateMarkerAndCircle(Position newLocalData, Uint8List imageData) {
    LatLng latlng = LatLng(newLocalData.latitude, newLocalData.longitude);
    this.setState(() {
      marker = Marker(
          markerId: MarkerId("home"),
          position: latlng,
          rotation: newLocalData.heading,
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(imageData));
      circle = Circle(
          circleId: CircleId("car"),
          radius: 20,
          zIndex: 1,
          strokeColor: Colors.blue,
          center: latlng,
          fillColor: Colors.blue.withAlpha(70));
    });
  }

  getCurrentLocation() async {
    try {
      Uint8List imageData = await getMarker();
      final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

      await geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((Position position) {
        setState(() {
          _currentPosition = position;
          latLngPosition = LatLng(position.latitude, position.longitude);
          mapController.animateCamera(CameraUpdate.newCameraPosition(
              CameraPosition(
                  bearing: 192.8334901395799,
                  target: LatLng(position.latitude, position.longitude),
                  tilt: 0,
                  zoom: 16.00)));
          updateMarkerAndCircle(position, imageData);
          print(_currentPosition.latitude);
          print(_currentPosition.longitude);
          getDistance(_currentPosition.latitude, _currentPosition.longitude);
        });
      }).catchError((e) {
        print(e);
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  getDistance(double latitude, double longitude) async {
    var dLat = radians(double.parse(platLoc) - latitude);
    var dLng = radians(double.parse(plngLoc) - longitude);

    print("cek");
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(radians(latitude)) *
            cos(radians(pLat)) *
            sin(dLng / 2) *
            sin(dLng / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var totalDistance = earthRadius * c;
    total = totalDistance;

    print(totalDistance); //d is the distance in meters
  }

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    _controllerGMap.complete(controller);
    await getCurrentLocation();

    print("cek");
  }

  @override
  void initState() {
    getCurrentLocation();
    getImage(ImageSource.camera);
    super.initState();
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.white, opacity: 1.0),
        title: Text("Presensi Pulang", style: TextStyle(color: Colors.white)),
        //leading: Icon(MdiIcons.accountBox, color: Colors.white),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              _image == null
                  ? Container(
                      height: 150.9,
                      width: 250.9,
                      child: Icon(
                        Icons.image,
                        size: 150.9,
                      ),
                    )
                  : Container(
                      height: 200.9,
                      //width: 250.9,
                      child: Image.file(
                        _image,
                        height: 150.9,
                        width: 250.9,
                        fit: BoxFit.cover,
                      )),
              SizedBox(
                height: 10,
              ),
              Text(
                "Lokasi Anda",
                style: TextStyle(color: Colors.black54),
              ),
              Container(
                margin: EdgeInsets.all(8),
                height: 300,
                color: Colors.blue,
                child: GoogleMap(
                  key: _mapKey,
                  mapType: MapType.normal,
                  initialCameraPosition: initialLocation,
                  //markers: Set.of((marker != null) ? [marker] : []),
                  circles: Set.of((circle != null) ? [circle] : []),
                  onMapCreated: _onMapCreated,
                ),
              ),
            ],
          )),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Divider(
                    color: Colors.black,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Jarak dari titik Presensi",
                    style: TextStyle(color: Colors.black54),
                  ),
                  Text(
                    "${total ?? 10.0000}" + " M",
                    style: TextStyle(fontSize: 16),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
          buttonPresensi(),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 50,
            ),
          )
        ],
      ),
    );
  }

  Widget buttonPresensi() {
    if (total == null) {
      return SliverToBoxAdapter(child: Container());
    } else if (total > 500.00) {
      return SliverToBoxAdapter(
        child: Container(
          margin: EdgeInsets.all(16),
          child: ElevatedButton(
              child: Text("Presensi".toUpperCase(),
                  style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  )),
              onPressed: () {
                Get.snackbar(
                  "GAGAL !!!",
                  "Anda harus berada di radius 10 M untuk presensi",
                  snackPosition: SnackPosition.BOTTOM,
                );
              },
              style: ElevatedButton.styleFrom(
                onPrimary: Colors.white,
                primary: Colors.red,
                minimumSize: Size(double.infinity, 50),
                padding: EdgeInsets.symmetric(horizontal: 16),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
              )),
        ),
      );
    } else {
      return SliverToBoxAdapter(
        child: Container(
          margin: EdgeInsets.all(16),
          child: ElevatedButton(
              child: Text("Presensi".toUpperCase(),
                  style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  )),
              onPressed: () {
                presensiController.upload(
                    _image,
                    token,
                    _currentPosition.latitude.toString(),
                    _currentPosition.longitude.toString(),
                    context,
                    1);
              },
              style: ElevatedButton.styleFrom(
                onPrimary: Colors.white,
                primary: Colors.green[800],
                minimumSize: Size(double.infinity, 50),
                padding: EdgeInsets.symmetric(horizontal: 16),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
              )),
        ),
      );
    }
  }

  Future getImage(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    final dir = await path_provider.getTemporaryDirectory();
    final targetPath = dir.absolute.path + "/temp.jpg";

    // File croppedFile = await ImageCropper.cropImage(
    //   sourcePath: pickedFile.path,
    //  aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
    //   maxWidth: 512,
    //   maxHeight: 512,
    // );

    var result = await FlutterImageCompress.compressAndGetFile(
      pickedFile.path,
      targetPath,
      minWidth: 1024,
      minHeight: 1024,
      quality: 50,
    );

    setState(() {
      if (pickedFile != null) {
        _image = result;
        print("SIZE  " + "${_image.lengthSync()}");
      } else {
        print('No image selected.');
      }
    });
  }
}
