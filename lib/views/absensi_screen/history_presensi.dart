import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/presensi_controller.dart';
import 'package:skuline/widget/date_card_row.dart';

class HistoryPresensiPage extends StatefulWidget {
  @override
  _HistoryPresensiPageState createState() => _HistoryPresensiPageState();
}

class _HistoryPresensiPageState extends State<HistoryPresensiPage> {
  PresensiController presensiController = Get.put(PresensiController());
  var token = Get.find<AuthController>().authModel.value.token;
  DateTime _dateReg;
  DateTime _dateSampai;

  DateTime _waktu;

  TimeOfDay jamBatasMasuk = TimeOfDay(hour: 07, minute: 0);
  TimeOfDay pulangCepat = TimeOfDay(hour: 13, minute: 0);
  TimeOfDay pulangTepat = TimeOfDay(hour: 15, minute: 0);

  @override
  void initState() {
    _dateReg = DateTime.now().subtract(Duration(days: 7));
    _dateSampai = DateTime.now();
    presensiController.historyPresensi(
        token,
        _dateReg.toIso8601String().substring(0, 10),
        _dateSampai.toIso8601String().substring(0, 10));
    print(_dateReg);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print("cek");
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.white, opacity: 1.0),
        title: Text("History Presensi", style: TextStyle(color: Colors.white)),
        //leading: Icon(MdiIcons.accountBox, color: Colors.white),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                Container(
                  height: 50,
                  width: size.width - 20,
                  margin: EdgeInsets.only(top: 8, left: 8, right: 8),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: InkWell(
                    onTap: () async {
                      DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: _dateReg,
                          firstDate: DateTime(2020),
                          lastDate: DateTime.now());

                      if (picked != null && picked != _dateReg) {
                        setState(() {
                          _dateReg = picked.add(Duration(hours: 17));
                          presensiController.historyPresensi(
                              token,
                              _dateReg.toIso8601String().substring(0, 10),
                              _dateSampai.toIso8601String().substring(0, 10));
                        });
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Dari Tanggal",
                          style: TextStyle(fontSize: 12),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.green[800])),
                          child: Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              DateCardRow(tanggal: _dateReg),
                              Icon(
                                Icons.arrow_drop_down,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: size.width - 20,
                  margin: EdgeInsets.only(top: 8, left: 8, right: 8),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: InkWell(
                    onTap: () async {
                      DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: _dateReg,
                          firstDate: DateTime(2020),
                          lastDate: DateTime.now());

                      if (picked != null && picked != _dateSampai) {
                        setState(() {
                          _dateSampai = picked.add(Duration(hours: 17));
                          presensiController.historyPresensi(
                              token,
                              _dateReg.toIso8601String().substring(0, 10),
                              _dateSampai.toIso8601String().substring(0, 10));
                        });
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Sampai Tanggal",
                          style: TextStyle(fontSize: 12),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 8, left: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.green[800])),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              DateCardRow(tanggal: _dateSampai),
                              Icon(
                                Icons.arrow_drop_down,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ])),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Riwayat Absensi Anda"),
            ),
          ),
          SliverToBoxAdapter(
            child: GetX<PresensiController>(
              init: Get.put<PresensiController>(PresensiController()),
              builder: (PresensiController presensiController) {
                if (presensiController != null &&
                    presensiController.presensiModel != null) {
                  print("cek");
                  return Container(
                    height: 500,
                    padding: EdgeInsets.only(bottom: 50),
                    child: ListView.builder(
                      itemCount: presensiController.presensiModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        String jam = presensiController
                            .presensiModel[index].waktu
                            .substring(11, 19);
                        print("cek");
                        _waktu = DateTime.parse(
                            presensiController.presensiModel[index].waktu);

                        var test = _waktu.minute - 7;
                        // var test = double.parse(jam);
                        print("cek");

                        

                        TimeOfDay jamMasuk = TimeOfDay.fromDateTime(_waktu);
                        return Container(
                          height: 85,
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    height: 50,
                                    width: 50,
                                    child: (presensiController
                                                .presensiModel[index]
                                                .fileName ==
                                            "Format file salah")
                                        ? Icon(Icons.image)
                                        : Image.network(
                                            "https://apisekolah.diengvalley.com/public/media/absensi_guru/" +
                                                presensiController
                                                    .presensiModel[index]
                                                    .fileName,
                                            fit: BoxFit.cover,
                                          ),
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Waktu",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2,
                                      ),
                                      SizedBox(height: 10),
                                      Text(jam,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2),
                                    ],
                                  ),
                                  Container(
                                    width: 140,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Tanggal",
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2,
                                        ),
                                        SizedBox(height: 10),
                                        DateCardRow(
                                          tanggal: _waktu,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Presensi",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2,
                                      ),
                                      SizedBox(height: 10),
                                      statusPresensi(
                                          pulangCepat,
                                          jamMasuk,
                                          jamBatasMasuk,
                                          presensiController
                                              .presensiModel[index].absenType)
                                    ],
                                  )
                                ],
                              ),
                              Divider()
                            ],
                          ),
                        );
                      },
                    ),
                  );
                } else {
                  return Container(
                      child: Center(child: CircularProgressIndicator()));
                }
              },
            ),
          )
        ],
      ),
    );
  }

  Widget statusPresensi(TimeOfDay pulangCepat, TimeOfDay jamPresensi,
      TimeOfDay batas, String type) {
    double batasJam = batas.hour.toDouble() + (batas.minute.toDouble() / 60);
    double presensi =
        jamPresensi.hour.toDouble() + (jamPresensi.minute.toDouble() / 60);
    double cepat =
        pulangCepat.hour.toDouble() + (pulangCepat.minute.toDouble() / 60);

    if (type == 'masuk') {
      if (presensi > batasJam) {
        return Text('T H');
      } else {
        return Text('H');
      }
    } else {
      if (presensi < cepat) {
        return Text('P C');
      } else {
        return Text('P T W');
      }
    }
  }
}
