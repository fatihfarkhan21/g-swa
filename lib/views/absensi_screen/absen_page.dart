import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/services/geolocation.dart';

import 'package:skuline/widget/date_card_row.dart';
import 'package:timeline_tile/timeline_tile.dart';

class AbsenPage extends StatefulWidget {
  @override
  _AbsenPageState createState() => _AbsenPageState();
}

class _AbsenPageState extends State<AbsenPage> {

  Position _currentPosition;

  List<Map<String, dynamic>> course = [
    {"text": "Absensi pulang", "jam": "08:00"},
    {"text": "Absensi pulang", "jam": "10:00"},
    {"text": "Absensi Datang", "jam": "13:00"},
    {"text": "Absensi Datang", "jam": "13:00"},
    {"text": "Absensi Datang", "jam": "13:00"},
    {"text": "Absensi pulang", "jam": "10:00"},
    {"text": "Absensi Datang", "jam": "13:00"},
    {"text": "Absensi pulang", "jam": "10:00"},
    {"text": "Absensi Datang", "jam": "13:00"},
  ];

  @override
  void initState() {
    super.initState();
  }

  GeolocationCal geo = GeolocationCal();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xFF004E92),
            Color(0xFF000428),
          ],
        ),
      ),  
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Column(
              children: <Widget>[
                const SizedBox(height: 16),
                const Text(
                  'History Presensi',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 16),
                Expanded(
                  child: ListView.builder(
                    itemCount: course.length,
                    itemBuilder: (BuildContext context, int index) {
                      final example = course[index];
                      return TimelineTile(
                        alignment: TimelineAlign.manual,
                        lineXY: 0.25,
                        isFirst: index == 0,
                        isLast: index == course.length - 1,
                        startChild: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(
                            example['jam'],
                            style: GoogleFonts.jura(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        indicatorStyle: IndicatorStyle(
                          width: 20,
                          height: 20,
                          indicator: _IndicatorExample(number: '${index + 1}'),
                          drawGap: true,
                        ),
                        beforeLineStyle: LineStyle(
                          color: Colors.white.withOpacity(0.2),
                        ),
                        endChild: GestureDetector(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                DateCardRow(
                                    tanggal: DateTime.now(),
                                    colors: Colors.white),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        example['text'],
                                        style: GoogleFonts.jura(
                                          color: Colors.white,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
          floatingActionButton: SpeedDial(
            marginEnd: 18,
            marginBottom: 20,
            icon: FontAwesomeIcons.clipboardCheck,
            activeIcon: Icons.close,
            buttonSize: 56.0,
            visible: true,
            closeManually: false,
            renderOverlay: false,
            curve: Curves.bounceIn,
            overlayColor: Colors.black,
            overlayOpacity: 0.5,
            tooltip: 'Speed Dial',
            heroTag: 'speed-dial-hero-tag',
            elevation: 8.0,
            foregroundColor: Colors.blue,
            onOpen: () {
              _getCurrentLocation();
            },
            shape: CircleBorder(),
            children: [
              SpeedDialChild(
                child: Icon(FontAwesomeIcons.check, color: Colors.white),
                backgroundColor: Colors.blue,
                label: 'Absen Pulang',
                labelStyle: TextStyle(fontSize: 18.0),
                onTap: () => print('SECOND CHILD'),
                onLongPress: () => print('SECOND CHILD LONG PRESS'),
              ),
              SpeedDialChild(
                child:
                    Icon(FontAwesomeIcons.clipboardList, color: Colors.white),
                backgroundColor: Colors.blue,
                label: 'Absen Datang',
                labelStyle: TextStyle(fontSize: 18.0),
                onTap: () {
                  //Get.to(AbsenDatang());
                },
                onLongPress: () => print('FIRST CHILD LONG PRESS'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() {
        _currentPosition = position;

        print(_currentPosition.latitude);
        print(_currentPosition.longitude);
        geo.getDistance(_currentPosition);
      });
    }).catchError((e) {
      print(e);
    });
  }
}

class _IndicatorExample extends StatelessWidget {
  const _IndicatorExample({Key key, this.number}) : super(key: key);

  final String number;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.fromBorderSide(
          BorderSide(
            color: Colors.white.withOpacity(0.2),
            width: 2,
          ),
        ),
      ),
      child: Center(
        child: Text(
          number,
          style: const TextStyle(fontSize: 12, color: Colors.white),
        ),
      ),
    );
  }
}
