import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/user_controller.dart';

class ProfilePage extends StatefulWidget {
  final BuildContext menuScreenContext;

  const ProfilePage({Key key, this.menuScreenContext}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  AuthController authController = Get.put(AuthController());
  var token = Get.find<AuthController>().authModel.value.token;
  TabController _tabController;
  ScrollController _scrollViewController;

  @override
  void initState() {
    super.initState();

    _scrollViewController = new ScrollController();
    _tabController = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        elevation: 3,
        iconTheme: IconThemeData(color: Colors.green[800], opacity: 1.0),
        title: Text("Profile".toUpperCase(),
            style: GoogleFonts.acme(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
            radius: 20,
            backgroundColor: Colors.green,
            child: Center(child: Icon(Icons.person)),
          ),
        ),
      ),
      body: GetX<UserController>(
          init: Get.put<UserController>(UserController()),
          builder: (UserController userController) {
            if (userController != null && userController.userModel != null) {
              return ListView(
                children: [
                  Container(
                    height: size.height / 6,
                    width: size.width,
                    padding: EdgeInsets.only(left: 8, top: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Stack(
                              children: [
                                CircleAvatar(
                                  radius: 50,
                                  backgroundColor: Colors.green[300],
                                  child: Center(
                                      child: Icon(
                                    Icons.image,
                                    color: Colors.white,
                                    size: 50,
                                  )),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  //left: 0,
                                  child: CircleAvatar(
                                    radius: 20,
                                    child: IconButton(
                                      icon: Icon(Icons.camera_enhance_rounded),
                                      onPressed: () {
                                        Get.find<AuthController>()
                                            .logout(token);
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${userController.userModel.value.pendudukName}",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                Text(
                                  "${userController.userModel.value.userName}",
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                                Text(
                                  "No Telp: ${userController.userModel.value.telp ?? "-"}",
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Divider(
                      thickness: 2,
                    ),
                  ),
                  Container(
                    color: Colors.green,
                    child: TabBar(
                      labelColor: Colors.white,
                      labelStyle: TextStyle(fontSize: 14),
                      tabs: <Widget>[
                        Tab(
                          // key: new Key('StatisticsTab'),
                          text: "Data Umun",
                        ),
                        Tab(
                          // key: new Key('HISTORY'),
                          text: "Alamat",
                        ),
                        Tab(
                          text: "Kepegawaian",
                          // key: new Key('HISTORY'),
                        )
                      ],
                      controller: _tabController,
                    ),
                  ),
                  Container(
                    height: size.height,
                    child: TabBarView(
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(
                                  "TTL",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.tempatLahir ?? "-"}" +
                                        "${userController.userModel.value.tanggalLahir ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Agama",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.agamaName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Jenis Kelamin",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.jenisKelaminName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "NIP",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.nip ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "NIK",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.nik ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Pendidikan",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.s1Jurusan ?? "-"}"),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(
                                  "Provinsi",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.provinsiName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Kabupaten",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.regencyName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Kecamatan",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.subRegency ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Desa",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.villageName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Jalan",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.alamat ?? "-"}"),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(
                                  "Jabatan",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.jabatanName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Golongan Ruangan",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.golRuangName ?? "-"}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "Kepegewaian",
                                  style: GoogleFonts.roboto(
                                      fontSize: 12,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                    "${userController.userModel.value.kepegawaianName ?? "-"}"),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                      ],
                      controller: _tabController,
                    ),
                  )
                ],
              );
            } else {
              return Container(
                  child: Center(child: CircularProgressIndicator()));
            }
          }),
    );
  }
}
