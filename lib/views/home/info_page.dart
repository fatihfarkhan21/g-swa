import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InfoPage extends StatelessWidget {
  final BuildContext menuScreenContext;

  const InfoPage({Key key, this.menuScreenContext}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Info".toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
        leading: Icon(FontAwesomeIcons.info, color: Colors.white),
        backgroundColor: Colors.green[800],
        centerTitle: true,
      ),
      body: Container(
          child: ListView.builder(
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            elevation: 3,
            margin: EdgeInsets.all(8),
            child: Container(
                margin: EdgeInsets.only(top: 8),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("Kamis, 22 april 2021",
                              style: TextStyle(fontWeight: FontWeight.w600))
                        ],
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.white,
                            child: Icon(Icons.info_outline, size: 50),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Lorem Ipsum",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 250,
                              height: 100,
                              padding: EdgeInsets.only(right: 16),
                              child: Text(
                                "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                                style: TextStyle(fontWeight: FontWeight.w400),
                                textAlign: TextAlign.justify,
                              ),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                )),
          );
        },
      )),
    );
  }
}
