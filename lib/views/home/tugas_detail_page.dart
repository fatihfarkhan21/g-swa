import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TugasDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("BIOLOGY X"),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
              child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Judul Tugas",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 20)),
                Text("Terakhir dikumpul pada April 25, 2021 23:59",
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14))
              ],
            ),
          )),
          SliverToBoxAdapter(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Instruksi"),
                SizedBox(
                    width: size.width - 16,
                    child: Text(
                        "1. Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n2. Lorem Ipsum is simply dummy text of the printing and typesetting industry."))
              ],
            ),
          )),
          SliverToBoxAdapter(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 32),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Tugas",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(height: 32),
                    Container(
                      height: 150,
                      width: 300,
                      decoration: BoxDecoration(
                        //color: Colors.blue,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: Colors.blue)
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.upload_file, color: Colors.blue),
                                SizedBox(width: 32),
                                Text("Pilih File", style: TextStyle(fontWeight: FontWeight.bold))
                              ],
                            ),
                          ),
                          Divider(color: Colors.blue),
                          Icon(FontAwesomeIcons.filePdf, color: Colors.blue, size: 60)
                        ],
                      ),
                    ),
                    SizedBox(height: 32),
                    OutlinedButton(
                        child: Text("Upload Tugas"), onPressed: () {})
                  ]),
            ),
          )
        ],
      ),
    );
  }
}
