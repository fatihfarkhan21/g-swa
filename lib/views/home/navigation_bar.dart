import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skuline/views/home/info_page.dart';
import 'package:skuline/views/home/user_home.dart';
import 'package:skuline/views/ujian_screen/ujian_page.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
      bottomNavigationBar: _bottomNavigator(),
    );
  }

  _bottomNavigator() {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      ),
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.blue,
        currentIndex: currentIndex,
        onTap: onTapped,
        items: [
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/icons/house.svg",
                  width: 35.0,
                  color: currentIndex == 0
                      ? Theme.of(context).accentColor
                      : Color(0xFF6C7174)),
              label: "Home"),
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/icons/read_book.svg",
                  width: 35.0,
                  color: currentIndex == 1
                      ? Theme.of(context).accentColor
                      : Color(0xFF6C7174)),
              label: "Ujian"),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                "assets/icons/comment.svg",
                width: 35.0,
                color: currentIndex == 2
                    ? Theme.of(context).accentColor
                    : Color(0xFF6C7174),
              ),
              label: "Info"),
        ],
      ),
    );
  }

  _buildBody() {
    if (currentIndex == 0) {
      return UserHome();
    } else if (currentIndex == 1) {
      return UjianPage();
    } else {
      return InfoPage();
    }
  }

  onTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
