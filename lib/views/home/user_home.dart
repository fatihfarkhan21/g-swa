import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:skuline/views/absensi_screen/absensi_pulang_page.dart';
import 'package:skuline/views/absensi_screen/history_presensi.dart';
import 'package:skuline/views/absensi_screen/presensi_datang_page.dart';
import 'package:skuline/views/home/profile_page.dart';
import 'package:skuline/views/school/school_home.dart';
import 'package:skuline/views/ujian_screen/ujian_page.dart';
import 'package:skuline/widget/redeem_Icon.dart';

class   UserHome extends StatelessWidget {
  final BuildContext menuScreenContext;

  const UserHome({Key key, this.menuScreenContext}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> course = [
      {
        "image": "assets/images/bg00.jpg",
        "text": "Biologi",  
        "jadwal": "08:00 - 10:00"
      },
      {
        "image": "assets/images/bg01.jpg",
        "text": "Akutansi",
        "jadwal": "10:00 - 12:00"
      },
      {
        "image": "assets/images/bg02.jpg",
        "text": "Bahasa Indonesia",
        "jadwal": "13:00 - 15:00"
      },
      {
        "image": "assets/images/bg02.jpg",
        "text": "Bahasa Indonesia",
        "jadwal": "13:00 - 15:00"
      },
      {
        "image": "assets/images/bg03.jpg",
        "text": "Bahasa Indonesia",
        "jadwal": "13:00 - 15:00"
      },
      {
        "image": "assets/images/bg01.jpg",
        "text": "Akutansi",
        "jadwal": "10:00 - 12:00"
      },
      {
        "image": "assets/images/bg00.jpg",
        "text": "Biologi",
        "jadwal": "08:00 - 10:00"
      },
      {
        "image": "assets/images/bg03.jpg",
        "text": "Bahasa Indonesia",
        "jadwal": "13:00 - 15:00"
      },
    ];
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.green[800],
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black45, opacity: 1.0),
          title: Text("BERANDA", style: TextStyle(color: Colors.white)),
          centerTitle: true,
          leading: Icon(FontAwesomeIcons.home, color: Colors.white),
        ),
        body: Column(
          children: [
            Card(
                elevation: 3,
                margin: EdgeInsets.all(8),
                child: Container(
                  height: 200,
                  padding: EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RedeemIcon(
                            icon: MdiIcons.login,
                            title: "Presensi Datang",
                            tap: () {
                              pushNewScreenWithRouteSettings(
                                context,
                                settings: RouteSettings(name: '/home'),
                                screen: PresensiDatangPage(),
                                pageTransitionAnimation:
                                    PageTransitionAnimation.slideUp,
                              );
                            },
                          ),
                          RedeemIcon(
                            icon: MdiIcons.schoolOutline,
                            title: "Tentang Sekolah",
                            tap: () {
                              pushNewScreenWithRouteSettings(
                                context,
                                settings: RouteSettings(name: '/home'),
                                screen: SchoolHome(),
                                pageTransitionAnimation:
                                    PageTransitionAnimation.slideUp,
                              );
                            },
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RedeemIcon(
                              icon: MdiIcons.logout,
                              title: "Presensi Pulang",
                              tap: () {
                                pushNewScreenWithRouteSettings(
                                  context,
                                  settings: RouteSettings(name: '/home'),
                                  screen: AbsensiPulang(),
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.slideUp,
                                );
                              }),
                          RedeemIcon(
                            icon: MdiIcons.fileOutline,
                            title: "Jadwal Ujian",
                            tap: () {
                               pushNewScreenWithRouteSettings(
                                context,
                                settings: RouteSettings(name: '/home'),
                                screen: UjianPage(),
                                pageTransitionAnimation:
                                    PageTransitionAnimation.slideUp,
                              );
                            },
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RedeemIcon(
                              icon: MdiIcons.history,
                              title: "History Presensi",
                              tap: () {
                                pushNewScreenWithRouteSettings(
                                  context,
                                  settings: RouteSettings(name: '/home'),
                                  screen: HistoryPresensiPage(),
                                  pageTransitionAnimation:
                                      PageTransitionAnimation.slideUp,
                                );
                              }),
                          RedeemIcon(
                            icon: MdiIcons.accountCircleOutline,
                            title: "Profile",
                            tap: () {
                              pushNewScreenWithRouteSettings(
                                context,
                                settings: RouteSettings(name: '/home'),
                                screen: ProfilePage(),
                                pageTransitionAnimation:
                                    PageTransitionAnimation.slideUp,
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                )),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(8),
                child: StaggeredGridView.countBuilder(
                  crossAxisCount: 4,
                  //shrinkWrap: true,
                  itemCount: course.length,
                  itemBuilder: (BuildContext context, int index) => Card(
                    //color: Colors.green,
                    elevation: 3,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          image: DecorationImage(
                              image: AssetImage(course[index]["image"]),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  staggeredTileBuilder: (int index) =>
                      StaggeredTile.count(2, index.isEven ? 2 : 1),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                ),
              ),
            )
          ],
        ));
  }
}
