import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:skuline/views/mapel/mapel_page.dart';
import 'package:skuline/views/home/profile_page.dart';
import 'package:skuline/views/home/user_home.dart';

class AppPage extends StatefulWidget {
  final BuildContext menuScreenContext;
  AppPage({Key key, this.menuScreenContext}) : super(key: key);

  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  PersistentTabController _controller;

  BuildContext testContext;

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 0);
  }

  List<Widget> _buildScreens() {
    return [
      UserHome(
        menuScreenContext: widget.menuScreenContext,
      ),
      MapelPage(
        menuScreenContext: widget.menuScreenContext,
      ),
      
      ProfilePage(
        menuScreenContext: widget.menuScreenContext,
      )
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(Icons.home),
        title: "Home",
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Colors.grey,
        inactiveColorSecondary: Colors.purple,
      ),
      // PersistentBottomNavBarItem(
      //   icon: Icon(Icons.assignment),
      //   title: ("Mapel"),
      //   activeColorPrimary: Colors.white,
      //   inactiveColorPrimary: Colors.grey,
      //   routeAndNavigatorSettings: RouteAndNavigatorSettings(),
      // ),
      // PersistentBottomNavBarItem(
      //     icon: Icon(FontAwesomeIcons.school),
      //     title: ("Add"),
      //     activeColorPrimary: Colors.green[800],
      //     activeColorSecondary: Colors.white,
      //     inactiveColorPrimary: Colors.white,
      //     onPressed: (context) {}),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.assignment),
        title: ("Mapel"),
        activeColorPrimary: Colors.green,
        activeColorSecondary: Colors.white,
        inactiveColorPrimary: Colors.white,
        routeAndNavigatorSettings: RouteAndNavigatorSettings(),
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.settings),
        title: ("Settings"),
        activeColorPrimary: Colors.white,
        inactiveColorPrimary: Colors.grey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PersistentTabView(
        context,
        controller: _controller,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.green[800],
        handleAndroidBackButtonPress: true,
        resizeToAvoidBottomInset: true,
        stateManagement: true,
        navBarHeight: MediaQuery.of(context).viewInsets.bottom > 0
            ? 0.0
            : kBottomNavigationBarHeight,
        hideNavigationBarWhenKeyboardShows: true,
        margin: EdgeInsets.all(0.0),
        popActionScreens: PopActionScreensType.all,
        bottomScreenMargin: 0.0,

        selectedTabScreenContext: (context) {
          testContext = context;
        },
        decoration: NavBarDecoration(
          colorBehindNavBar: Colors.indigo,
        ),
        popAllScreensOnTapOfSelectedTab: true,
        itemAnimationProperties: ItemAnimationProperties(
          duration: Duration(milliseconds: 400),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: ScreenTransitionAnimation(
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        navBarStyle:
            NavBarStyle.style17, // Choose the nav bar style with this property
      ),
    );
  }
}
