import 'package:flutter/material.dart';
import 'package:skuline/views/home/info_page.dart';
import 'package:skuline/views/home/profile_page.dart';
import 'package:skuline/views/home/user_home.dart';

class TabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigationKey;
  final String tabItem;
  const TabNavigator({Key key, this.navigationKey, this.tabItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget child;

    if (tabItem == "page1")
      child = UserHome();
    else if (tabItem == "page2")
      child = ProfilePage();
    else if (tabItem == "page3") child = InfoPage();
    return Navigator(
      key: navigationKey,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
            builder: (context) => Container(
                  child: child,
                ));
      },
    );
  }
}
