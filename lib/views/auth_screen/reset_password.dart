import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/widget/card/iklan_slider.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();
  AuthController authController = Get.put(AuthController());

  String email;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Stack(children: [Container(), IklanSlider()]),
          Positioned(
            top: 200,
            right: 0,
            left: 0,
            child: Card(
              margin: EdgeInsets.all(16),
              elevation: 3,
              child: Container(
                  padding: EdgeInsets.only(bottom: 16, top: 16),
                  width: double.infinity,
                  child: SingleChildScrollView(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: [
                        Text("Reset Password",
                            style: GoogleFonts.roboto(
                              color: Colors.blue,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            )),
                        SizedBox(
                          height: size.height / 12,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              TextFormField(
                                onSaved: (value) => email = value,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  labelText: "Email",
                                  hintText: "Masukan Email Anda",
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 42, vertical: 20),
                                  suffixIcon:
                                      Icon(Icons.mail, color: Colors.blue),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  child: Text("KIRIM",
                                      style: GoogleFonts.roboto(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1)),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      authController.resetPassword(email);
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    onPrimary: Colors.white,
                                    primary: Colors.blue,
                                    minimumSize: Size(double.infinity, 50),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16),
                                    shape: const RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                    ),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ))),
            ),
          ),
        ],
      ),
    );
  }
}
