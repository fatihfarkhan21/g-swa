import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/views/auth_screen/signIn_page.dart';
import 'package:skuline/views/home/app_page.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("cek");
    return GetX<AuthController>(
        init: Get.put<AuthController>(AuthController()),
        builder: (AuthController authController) {
           print("cek");
          if (authController != null && authController.authModel.value.pesan != false) {
            return AppPage();
          } else {
            return SignInPage();
          }
        });
  }
}
