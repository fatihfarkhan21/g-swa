import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/views/auth_screen/reset_password.dart';
import 'package:skuline/widget/card/iklan_slider.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _formKey = GlobalKey<FormState>();
  AuthController authController = Get.put(AuthController());

  String email;
  String password;
  bool obscureText = true;
  IconData iconPass = Icons.remove_red_eye;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Stack(children: [Container(), IklanSlider()]),
          Positioned(
            top: 200,
            right: 0,
            left: 0,
            child: Card(
              margin: EdgeInsets.all(16),
              elevation: 3,
              child: Container(
                  padding: EdgeInsets.only(bottom: 16, top: 16),
                  width: double.infinity,
                  child: SingleChildScrollView(
                      child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: [
                        // SizedBox(
                        //   height: size.height / 12,
                        // ),
                        Text("SIGN IN",
                            style: GoogleFonts.roboto(
                              color: Colors.green[800],
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            )),
                        Text("Swadaya Online School",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                              color: Colors.green[800],
                              fontSize: 25,
                              fontWeight: FontWeight.w500,
                            )),
                        SizedBox(
                          height: size.height / 12,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              TextFormField(
                                onSaved: (value) => email = value,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  labelText: "NISN",
                                  hintText: "Masukan NISN Anda",
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  // enabledBorder: OutlineInputBorder(
                                  //   borderRadius: BorderRadius.circular(20),
                                  //   borderSide: BorderSide(color: Colors.blue),
                                  // ),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 42, vertical: 20),
                                  suffixIcon:
                                      Icon(Icons.mail, color: Colors.green[800]),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                onSaved: (value) => password = value,
                                keyboardType: TextInputType.emailAddress,
                                obscureText: obscureText,
                                decoration: InputDecoration(
                                  labelText: "Password",
                                  hintText: "Masukan Password Anda",
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 42, vertical: 20),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      if (obscureText == true) {
                                        setState(() {
                                          obscureText = false;

                                          iconPass = Icons.remove_red_eye;
                                        });
                                      } else {
                                        setState(() {
                                          obscureText = true;

                                          iconPass = Icons.visibility_off;
                                        });
                                      }
                                    },
                                    icon: Icon(iconPass, color: Colors.green[800]),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  InkWell(
                                    onTap: (){
                                      Get.to(ResetPassword());
                                    },
                                    child: Text("Lupa password ?",
                                        style: GoogleFonts.roboto(
                                          color: Colors.green[800],
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                        )),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  child: Text("SIGN IN",
                                      style: GoogleFonts.roboto(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      )),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      authController.login(email, password);
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    onPrimary: Colors.white,
                                    primary: Colors.green[800],
                                    minimumSize: Size(double.infinity, 50),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16),
                                    shape: const RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                    ),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ))),
            ),
          ),
        ],
      ),
    );
  }
}
