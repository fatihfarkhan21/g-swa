
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:skuline/views/ujian_screen/uas_page.dart';
import 'package:skuline/views/ujian_screen/ujian_sekolah_page.dart';

import 'mid_page.dart';

class UjianPage extends StatefulWidget {
  @override
  _UjianPageState createState() => _UjianPageState();
}

class _UjianPageState extends State<UjianPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  ScrollController _scrollViewController;

  @override
  void initState() {
    super.initState();
    _scrollViewController = new ScrollController();
    _tabController = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
      controller: _scrollViewController,
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            title: Text(
              "Ujian".toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
            leading: Icon(FontAwesomeIcons.bookOpen, color: Colors.white),
            backgroundColor: Colors.green[800],
            centerTitle: true,
            pinned: true,
            floating: true,
            forceElevated: innerBoxIsScrolled,
            bottom: TabBar(
              labelColor: Colors.white,
              labelStyle: TextStyle(fontSize: 14),
              tabs: <Widget>[
                Tab(
                  key: new Key('StatisticsTab'),
                  text: "UAS",
                  //icon: new Icon(Icons.assignment, color: Colors.white),
                ),
                Tab(
                  key: new Key('HISTORY'),
                  text: "MID",
                  //icon: new Icon(Icons.assignment, color: Colors.white),
                ),
                Tab(
                  key: new Key('HISTORY'),
                  text: "UJIAN SEKOLAH",
                  // icon: new Icon(
                  //   Icons.assignment,
                  //   color: Colors.white,
                  // ),
                )
              ],
              controller: _tabController,
            ),
          )
        ];
      },
      body: TabBarView(
        children: <Widget>[
          UasPage(),
          MidPage(),
          UjianSekolahPage(),
        ],
        controller: _tabController,
      ),
    ));
  }
}
