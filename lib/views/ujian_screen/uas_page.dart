import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/controllers.dart';
import 'package:skuline/models/jadwal_ujian_akhir_model.dart';
import 'package:skuline/models/models.dart';
import 'package:skuline/widget/card/card_jadwal_ujian.dart';

class UasPage extends StatefulWidget {
  const UasPage({Key key}) : super(key: key);

  @override
  _UasPageState createState() => _UasPageState();
}

class _UasPageState extends State<UasPage> with SingleTickerProviderStateMixin {
  JadwalUjianController jadwalUjianController =
      Get.find<JadwalUjianController>();
  var token = Get.find<AuthController>().authModel.value.token;
  String valueChoose;
  String semester;
  DateTime _tanggal;

  static String baseUrl = "https://apisekolah.diengvalley.com/";
  List dataTh = List.empty();
  List dataSemester = List.empty();

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<List<TahunAjaran>> getTahunAjaran(String token) async {
    try {
      var response =
          await dio.post('tahun_ajaran/data', data: {"token": token});
      List res = json.decode(response.data);

      print("cek");

      List<TahunAjaran> th = res.map((hP) => TahunAjaran.fromMap(hP)).toList();
      setState(() {
        dataTh = th;
      });

      return dataTh;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<List<Semester>> getSemester(String token, String id) async {
    try {
      var response = await dio.post('tahun_ajaran/semester_by_tahun_ajaran_id',
          data: {"token": token, "tahun_ajaran_id": id});
      List res = json.decode(response.data);

      print("cek");

      List<Semester> th = res.map((hP) => Semester.fromMap(hP)).toList();
      setState(() {
        dataSemester = th;
      });

      return dataSemester;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  @override
  void initState() {
    this.getTahunAjaran(token);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 150,
                  height: 30,
                  padding: const EdgeInsets.only(left: 8),
                  margin: EdgeInsets.only(left: 8, right: 8),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1),
                      borderRadius: BorderRadius.circular(5)),
                  child: DropdownButton(
                    hint: Text(
                      "Tahun Ajaran",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                    ),
                    focusColor: Colors.white,
                    icon: Icon(Icons.arrow_drop_down),
                    isExpanded: true,
                    underline: SizedBox(),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                    ),
                    value: valueChoose,
                    onChanged: (newValue) {
                      //jadwalUjianController.getSemester(token, newValue);
                      setState(() {
                        valueChoose = newValue;
                        this.getSemester(token, valueChoose);

                        semester = null;
                      });

                      //semester = null;
                    },
                    items: dataTh.map((valueItem) {
                      return DropdownMenuItem(
                        value: valueItem.tahunAjaranId,
                        child: Text(valueItem.tahunAjaranName),
                      );
                    }).toList(),
                  ),
                ),
                Container(
                  width: 150,
                  height: 30,
                  padding: const EdgeInsets.only(left: 8),
                  margin: EdgeInsets.only(right: 8),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1),
                      borderRadius: BorderRadius.circular(5)),
                  child: DropdownButton(
                    hint: Text(
                      "Semester",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                    ),
                    icon: Icon(Icons.arrow_drop_down),
                    isExpanded: true,
                    underline: SizedBox(),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                    ),
                    value: semester,
                    onChanged: (newValue) {
                      setState(() {
                        semester = newValue;
                        jadwalUjianController.getJadwalAkhirSmstr(
                            token, semester);
                          print("cek");
                      });
                    },
                    items: dataSemester.map((valueItem) {
                      print("cek");
                      return DropdownMenuItem(
                        value: valueItem.semesterId,
                        child: Text(valueItem.semesterName),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 8),
            Container(
                height: size.height - 100,
                child: GetX<JadwalUjianController>(
                    init:
                        Get.put<JadwalUjianController>(JadwalUjianController()),
                    builder: (JadwalUjianController jadwalUjianController) {
                      return ListView.builder(
                        itemCount:
                            jadwalUjianController.jadwalUjianAkhirModel.length,
                        itemBuilder: (BuildContext context, int index) {
                           print("cek");
                          _tanggal = DateTime.parse(jadwalUjianController
                              .jadwalUjianAkhirModel[index].tanggal);
                          var _susulan = DateTime.parse(jadwalUjianController
                              .jadwalUjianAkhirModel[index].tanggalUlang);
                          var _remed = DateTime.parse(jadwalUjianController
                              .jadwalUjianAkhirModel[index].tanggaRemidi);
                          var validasi = jadwalUjianController
                              .jadwalUjianAkhirModel[index].tanggaRemidi;

                          var validasiSusulan = jadwalUjianController
                              .jadwalUjianAkhirModel[index].tanggalUlang;
                          JadwalUjianAkhirModel jadwalUjianAkhirModel =
                              jadwalUjianController
                                  .jadwalUjianAkhirModel[index];
                                  print("cek");
                          if (jadwalUjianController
                                  .jadwalUjianAkhirModel.length == 0) {
                            return Container(
                              child: Center(
                                child: Text("Kosong"),
                              ),
                            );
                          } else {
                            return CardUjian(
                              tanggal: _tanggal,
                              validasiSusulan: validasiSusulan,
                              susulan: _susulan,
                              validasi: validasi,
                              remed: _remed,
                              ujianAkhirModel: jadwalUjianAkhirModel,
                            );
                          }
                        },
                      );
                    })),
          ],
        ),
      ),
    ));
  }
}
