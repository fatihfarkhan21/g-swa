
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/controllers.dart';
import 'package:skuline/widget/date_card_tall.dart';

class UjianSekolahPage extends StatefulWidget {
  const UjianSekolahPage({Key key}) : super(key: key);

  @override
  _UjianSekolahPageState createState() => _UjianSekolahPageState();
}

class _UjianSekolahPageState extends State<UjianSekolahPage> with SingleTickerProviderStateMixin {
  JadwalUjianController jadwalUjianController =
      Get.find<JadwalUjianController>();
  var token = Get.find<AuthController>().authModel.value.token;
  DateTime _tanggal;
  

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                height: size.height - 250,
                child: GetX<JadwalUjianController>(
                    init:
                        Get.put<JadwalUjianController>(JadwalUjianController()),
                    builder: (JadwalUjianController jadwalUjianController) {
                      return ListView.builder(
                        itemCount:
                            jadwalUjianController.jadwalUjianSekolahModel.length,
                        itemBuilder: (BuildContext context, int index) {
                          _tanggal = DateTime.parse(jadwalUjianController
                              .jadwalUjianSekolahModel[index].tanggal);
                          return Card(
                            child: Container(
                              height: 150,
                              child: Column(
                                children: [
                                  Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8),
                                      height: 32,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5),
                                          topRight: Radius.circular(5),
                                        ),
                                        color: Colors.green[800],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          "${jadwalUjianController.jadwalUjianSekolahModel[index].kodeJadwal ?? " "}",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )),
                                  SizedBox(height: 8),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        DateCardTall(
                                          tanggal: _tanggal,
                                        ),
                                        SizedBox(width: 8),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(
                                              width: 250,
                                              child: Text(
                                                  "${jadwalUjianController.jadwalUjianSekolahModel[index].namaMapel ?? " "}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline6),
                                            ),
                                            SizedBox(height: 8),
                                            Container(
                                              height: 32,
                                              padding: const EdgeInsets.all(8),
                                              // width: 200,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: Colors.green[50],
                                              ),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "${jadwalUjianController.jadwalUjianSekolahModel[index].jamMulai ?? " "}",
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                  Text(
                                                    " - ",
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                  Text(
                                                    "${jadwalUjianController.jadwalUjianSekolahModel[index].jamSelesai ?? " "}",
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    })),
          ],
        ),
      ),
    ));
  }
}
