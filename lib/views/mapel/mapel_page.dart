import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/jadwal_mapel.dart';
import 'package:skuline/models/semester.dart';
import 'package:skuline/models/tahun_ajaran.dart';
import 'package:skuline/widget/card/card_jadwal_mapel.dart';

class MapelPage extends StatefulWidget {
  final BuildContext menuScreenContext;

  const MapelPage({Key key, this.menuScreenContext}) : super(key: key);
  @override
  _MapelPageState createState() => _MapelPageState();
}

class _MapelPageState extends State<MapelPage> {
  JadwalMapelController jadwalMapelController =
      Get.find<JadwalMapelController>();
  var token = Get.find<AuthController>().authModel.value.token;
  String valueChoose;
  DateTime _tanggal;
  String semester;

  static String baseUrl = "https://apisekolah.diengvalley.com/";
  List dataTh = List.empty();
  List dataSemester = List.empty();

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<List<TahunAjaran>> getTahunAjaran(String token) async {
    try {
      var response =
          await dio.post('tahun_ajaran/data', data: {"token": token});
      List res = json.decode(response.data);

      print("cek");

      List<TahunAjaran> th = res.map((hP) => TahunAjaran.fromMap(hP)).toList();
      setState(() {
        dataTh = th;
      });

      return dataTh;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<List<Semester>> getSemester(String token, String id) async {
    try {
      var response = await dio.post('tahun_ajaran/semester_by_tahun_ajaran_id',
          data: {"token": token, "tahun_ajaran_id": id});
      List res = json.decode(response.data);

      print("cek");

      List<Semester> th = res.map((hP) => Semester.fromMap(hP)).toList();
      setState(() {
        dataSemester = th;
      });

      return dataSemester;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  @override
  void initState() {
    this.getTahunAjaran(token);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black45, opacity: 1.0),
        title: Text("Jadwal Mapel".toUpperCase(),
            style: TextStyle(color: Colors.white)),
        centerTitle: true,
        leading: Icon(FontAwesomeIcons.home, color: Colors.white),
      ),
      body: SingleChildScrollView(
        child: Container(
          //color: Colors.green[800],
          child: Column(
            children: [
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 150,
                      height: 30,
                      padding: const EdgeInsets.only(left: 8),
                      margin: EdgeInsets.only(left: 8, right: 8, bottom: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(5)),
                      child: DropdownButton(
                        hint: Text(
                          "Tahun Ajaran",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                        focusColor: Colors.white,
                        icon: Icon(Icons.arrow_drop_down),
                        isExpanded: true,
                        underline: SizedBox(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                        ),
                        value: valueChoose,
                        onChanged: (newValue) {
                          //jadwalUjianController.getSemester(token, newValue);
                          setState(() {
                            valueChoose = newValue;
                            this.getSemester(token, valueChoose);

                            semester = null;
                          });

                          //semester = null;
                        },
                        items: dataTh.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem.tahunAjaranId,
                            child: Text(valueItem.tahunAjaranName),
                          );
                        }).toList(),
                      ),
                    ),
                    Container(
                      width: 150,
                      height: 30,
                      padding: const EdgeInsets.only(left: 8),
                      margin: EdgeInsets.only(right: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(5)),
                      child: DropdownButton(
                        hint: Text(
                          "Semester",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                        icon: Icon(Icons.arrow_drop_down),
                        isExpanded: true,
                        underline: SizedBox(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                        ),
                        value: semester,
                        onChanged: (newValue) {
                          //jadwalUjianController.getSemester(token, newValue);
                          setState(() {
                            semester = newValue;
                            jadwalMapelController.getJadwalMapel(
                                token, semester);
                          });
                        },
                        items: dataSemester.map((valueItem) {
                          print("cek");
                          return DropdownMenuItem(
                            value: valueItem.semesterId,
                            child: Text(valueItem.semesterName),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              ),
              Obx(() => Container(
                    height: size.height - 170,
                    child: ListView.builder(
                      itemCount: jadwalMapelController.jadwalMapelModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        _tanggal = DateTime.parse(jadwalMapelController
                            .jadwalMapelModel[index].tanggalMulai);

                        return CardMapel(
                          jadwalMapel:
                              jadwalMapelController.jadwalMapelModel[index],
                        );
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
