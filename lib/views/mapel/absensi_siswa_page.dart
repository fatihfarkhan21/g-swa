import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/jadwal_mapel.dart';
import 'package:skuline/models/pertemuan_model.dart';
import 'package:skuline/widget/date_card_row.dart';

class AbsensiSiswaPage extends StatefulWidget {
  final Pertemuan pertemuan;
  final String namaMapel;
  const AbsensiSiswaPage({Key key, this.pertemuan, this.namaMapel})
      : super(key: key);

  @override
  _AbsensiSiswaPageState createState() => _AbsensiSiswaPageState();
}

class _AbsensiSiswaPageState extends State<AbsensiSiswaPage> {
  DateTime _tanggal;
  JadwalMapelController jadwalMapelController =
      Get.find<JadwalMapelController>();
  var token = Get.find<AuthController>().authModel.value.token;
  @override
  void initState() {
    _tanggal = DateTime.now();
    jadwalMapelController.getDaftarSiswa(token, widget.pertemuan.pertemuanId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("cek");
    var tanggal = DateTime.parse(widget.pertemuan.tanggalPertemuan);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.namaMapel.toUpperCase(),
            style: GoogleFonts.acme(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        backgroundColor: Colors.green[800],
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                //height: 200,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 30,
                          //margin: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.green),
                          child: Center(
                            child: Text(
                                widget.namaMapel.substring(0, 2).toUpperCase(),
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 260,
                              child: Text(widget.pertemuan.namaPertemuan,
                                  style: Theme.of(context).textTheme.subtitle1),
                            ),
                            DateCardRow(
                              tanggal: tanggal,
                              colors: Colors.black54,
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 150,
                      child: Text(widget.pertemuan.deskripsi,
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                    // SizedBox(
                    //   height: 16,
                    // ),
                  ],
                ),
              ),
              Obx(() => (Expanded(
                      child: Container(
                    color: Colors.grey[100],
                    child: ListView.builder(
                      itemCount: jadwalMapelController.daftarSiswa.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                         // height: size.height / 8,
                          padding: EdgeInsets.all(8),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CircleAvatar(
                                    child: Text(
                                      '${jadwalMapelController.daftarSiswa[index].pendudukName.substring(0, 2)}'
                                          .toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    backgroundColor: Colors.green,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 250,
                                        child: Text(
                                          "${jadwalMapelController.daftarSiswa[index].pendudukName}",
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: false,
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      statusAbsenSiswa(jadwalMapelController
                                          .daftarSiswa[index].statusPresensi)
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Alert(
                                        context: context,
                                        //type: AlertType.warning,
                                        title: "Absensi",
                                        desc: "Absensi Siswa",
                                        buttons: [
                                          DialogButton(
                                            child: Text(
                                              "Absen",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                            onPressed: () {
                                              jadwalMapelController
                                                  .updateAbsenSiswa(
                                                      token,
                                                      jadwalMapelController
                                                          .daftarSiswa[index]
                                                          .absensiSiswaId,
                                                      "3");
                                              setState(() {
                                                jadwalMapelController
                                                    .getDaftarSiswa(
                                                        token,
                                                        widget.pertemuan
                                                            .pertemuanId);
                                              });
                                              Navigator.pop(context);
                                            },
                                            color: Colors.red,
                                          ),
                                          DialogButton(
                                            child: Text(
                                              "Izin",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                            onPressed: () {
                                              jadwalMapelController
                                                  .updateAbsenSiswa(
                                                      token,
                                                      jadwalMapelController
                                                          .daftarSiswa[index]
                                                          .absensiSiswaId,
                                                      "2");
                                              Navigator.pop(context);
                                              setState(() {
                                                jadwalMapelController
                                                    .getDaftarSiswa(
                                                        token,
                                                        widget.pertemuan
                                                            .pertemuanId);
                                              });
                                            },
                                            color: Colors.blue,
                                          ),
                                          DialogButton(
                                            child: Text(
                                              "Masuk",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                            onPressed: () {
                                              jadwalMapelController
                                                  .updateAbsenSiswa(
                                                      token,
                                                      jadwalMapelController
                                                          .daftarSiswa[index]
                                                          .absensiSiswaId,
                                                      "1");
                                              Navigator.pop(context);
                                              setState(() {
                                                jadwalMapelController
                                                    .getDaftarSiswa(
                                                        token,
                                                        widget.pertemuan
                                                            .pertemuanId);
                                              });
                                            },
                                            color: Colors.green,
                                          )
                                        ],
                                      ).show();
                                    },
                                    child: Icon(
                                      Icons.more_vert,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              Divider(color: Colors.black54)
                            ],
                          ),
                        );
                      },
                    ),
                  ))))
            ],
          ),
        ],
      ),
    );
  }

  Widget statusAbsenSiswa(String status) {
    if (status == "0") {
      return Container(
        height: 25,
        width: 70,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.green,
        ),
        child: Center(
          child: Text(
            "Belum",
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.center,
          ),
        ),
      );
    } else if (status == "1") {
      return Container(
        height: 25,
        width: 70,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.green,
        ),
        child: Center(
          child: Text(
            "Hadir",
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.center,
          ),
        ),
      );
    } else if (status == "2") {
      return Container(
        height: 25,
        width: 70,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.blue,
        ),
        child: Center(
          child: Text(
            "Izin",
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.center,
          ),
        ),
      );
    } else {
      return Container(
        height: 25,
        width: 70,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.red,
        ),
        child: Center(
          child: Text(
            "Absen",
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }
}
