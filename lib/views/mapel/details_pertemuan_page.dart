import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_4.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/jadwal_mapel.dart';
import 'package:skuline/controllers/user_controller.dart';
import 'package:skuline/models/pertemuan_model.dart';
import 'package:skuline/widget/date_card_row.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsPertemuanPage extends StatefulWidget {
  final Pertemuan pertemuan;
  final String namaMapel;
  const DetailsPertemuanPage({Key key, this.pertemuan, this.namaMapel})
      : super(key: key);

  @override
  _DetailsPertemuanPageState createState() => _DetailsPertemuanPageState();
}

class _DetailsPertemuanPageState extends State<DetailsPertemuanPage> {
  // Future<void> _launched;
  // String _phone = '';

  TextEditingController komentarTC = TextEditingController();
  String komentar;
  final _formKey = GlobalKey<FormState>();
  JadwalMapelController jadwalMapelController =
      Get.find<JadwalMapelController>();
  var token = Get.find<AuthController>().authModel.value.token;
  UserController user = Get.find<UserController>();

  @override
  void initState() {
    jadwalMapelController.getDaftarMateri(token, widget.pertemuan.pertemuanId);
    jadwalMapelController.getDaftarKomentar(
        token, widget.pertemuan.pertemuanId);
    user.getUser(token);
    super.initState();
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  // getPermission() async {
  //   await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  // }

  @override
  Widget build(BuildContext context) {
    DateTime tanggal = DateTime.parse(widget.pertemuan.tanggalPertemuan);

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.namaMapel.toUpperCase(),
            style: GoogleFonts.acme(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        backgroundColor: Colors.green[800],
        elevation: 3,
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                //height: 200,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          width: 30,
                          //margin: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.green),
                          child: Center(
                            child: Text(
                                widget.namaMapel.substring(0, 2).toUpperCase(),
                                style: Theme.of(context).textTheme.bodyText1),
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.pertemuan.namaPertemuan,
                                style: Theme.of(context).textTheme.bodyText2),
                            DateCardRow(tanggal: tanggal)
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(widget.pertemuan.deskripsi,
                        style: Theme.of(context).textTheme.subtitle1),
                    Obx(() => (Column(
                        children: List.generate(
                            jadwalMapelController.daftarMateri.length,
                            (index) => (jadwalMapelController
                                        .daftarMateri[index].file ==
                                    null)
                                ? Container(
                                    height: 1,
                                  )
                                : Container(
                                    margin: EdgeInsets.symmetric(vertical: 8),
                                    height: 35,
                                    width: size.width,
                                    //color: Colors.green,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              // margin: EdgeInsets.symmetric(vertical: 4),
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.green[800],
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                              ),
                                              child: Center(
                                                  child: Icon(
                                                FontAwesomeIcons.fileDownload,
                                                color: Colors.white,
                                                size: 15,
                                              )),
                                            ),
                                            SizedBox(
                                              width: 8,
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "${jadwalMapelController.daftarMateri[index].judulFile}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2,
                                                ),
                                                Text(
                                                    "${jadwalMapelController.daftarMateri[index].keteranganMateri}"),
                                              ],
                                            ),
                                          ],
                                        ),
                                        IconButton(
                                          onPressed: () async {
                                            String url =
                                                "https://smk-swadaya.com/public/media/file_materi/" +
                                                    jadwalMapelController
                                                        .daftarMateri[index]
                                                        .file;
                                            _launchInBrowser(url);
                                          },
                                          icon: Icon(
                                            Icons.download,
                                            color: Colors.black54,
                                          ),
                                        ),
                                      ],
                                    ))))))
                  ],
                ),
              ),
              Expanded(
                  child: Obx(() => (Container(
                        padding: EdgeInsets.only(
                          bottom: 60,
                        ),
                        color: Colors.grey[100],
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8, left: 8.0, bottom: 8),
                              child: Text(
                                "Komentar",
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Divider(color: Colors.black54),
                            ),
                            Expanded(
                                child: ListView.builder(
                              itemCount:
                                  jadwalMapelController.daftarKomen.length,
                              itemBuilder: (BuildContext context, int index) {
                                print(user.userModel.value.userId);
                                DateTime tanggalKomen = DateTime.parse(
                                    jadwalMapelController
                                        .daftarKomen[index].createdTime);
                                return (jadwalMapelController
                                            .daftarKomen[index].userId ==
                                        user.userModel.value.userId)
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: ChatBubble(
                                          clipper: ChatBubbleClipper4(
                                              type: BubbleType.sendBubble),
                                          backGroundColor: Colors.blue,
                                          alignment: Alignment.topRight,
                                          margin: EdgeInsets.only(top: 10),
                                          child: Stack(
                                            children: [
                                              Container(
                                                constraints: BoxConstraints(
                                                  maxWidth:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.7,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        SizedBox(
                                                          width: 120,
                                                          child: Text(
                                                            "${jadwalMapelController.daftarKomen[index].pendudukName}",
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 12),
                                                          ),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            DateCardRow(
                                                              tanggal:
                                                                  tanggalKomen,
                                                              colors: Colors
                                                                  .black54,
                                                              size: 10,
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 30.0),
                                                      child: Text(
                                                        "${jadwalMapelController.daftarKomen[index].isiKomentar}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Positioned(
                                                  bottom: 0,
                                                  right: 0,
                                                  //left: 0,
                                                  child: Text(
                                                    "${jadwalMapelController.daftarKomen[index].createdTime.substring(11, 16)}",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 12),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      )
                                    : Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: ChatBubble(
                                          clipper: ChatBubbleClipper4(
                                              type: BubbleType.receiverBubble),
                                          backGroundColor: Color(0xffE7E7ED),
                                          margin: EdgeInsets.only(top: 10),
                                          child: Stack(
                                            children: [
                                              Container(
                                                constraints: BoxConstraints(
                                                  maxWidth:
                                                      MediaQuery.of(context)
                                                              .size
                                                              .width *
                                                          0.7,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        SizedBox(
                                                          width: 120,
                                                          child: Text(
                                                            "${jadwalMapelController.daftarKomen[index].pendudukName}",
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.blue,
                                                                fontSize: 12),
                                                          ),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            DateCardRow(
                                                              tanggal:
                                                                  tanggalKomen,
                                                              colors: Colors
                                                                  .black54,
                                                              size: 10,
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 8,
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 30.0),
                                                      child: Text(
                                                        "${jadwalMapelController.daftarKomen[index].isiKomentar}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Positioned(
                                                  bottom: 0,
                                                  right: 0,
                                                  //left: 0,
                                                  child: Text(
                                                    "${jadwalMapelController.daftarKomen[index].createdTime.substring(11, 16)}",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      );
                              },
                            )),
                          ],
                        ),
                      ))))
            ],
          ),
          Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Container(
                height: 60,
                width: size.width,
                color: Colors.grey[300],
                //margin: EdgeInsets.all(16),
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 40,
                      width: size.width - 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white),
                      child: Form(
                        key: _formKey,
                        child: TextFormField(
                          onSaved: (val) => komentar = val,
                          controller: komentarTC,
                          maxLines: 3,
                          minLines: 1,
                          cursorColor: Colors.black54,
                          decoration: InputDecoration(
                              hintText: 'Reply',
                              contentPadding: EdgeInsets.only(left: 8)),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          jadwalMapelController.createKomentar(
                              token, widget.pertemuan.pertemuanId, komentar);
                        }
                        setState(() {
                          komentarTC.clear();
                        });
                        jadwalMapelController.getDaftarKomentar(
                            token, widget.pertemuan.pertemuanId);
                      },
                      child: Icon(
                        Icons.send,
                        color: Colors.green[800],
                      ),
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
