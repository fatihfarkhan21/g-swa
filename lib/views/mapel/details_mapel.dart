import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/jadwal_mapel.dart';
import 'package:skuline/models/pertemuan_model.dart';
import 'package:skuline/widget/card/card_pertemuan.dart';
import 'package:skuline/widget/date_card_row.dart';

class DetailsMapel extends StatefulWidget {
  final String pelajaranId;
  final String namaMapel;
  const DetailsMapel({Key key, this.pelajaranId, this.namaMapel})
      : super(key: key);

  @override
  _DetailsMapelState createState() => _DetailsMapelState();
}

class _DetailsMapelState extends State<DetailsMapel> {
  final _formKey = GlobalKey<FormState>();
  String namaPertemuan;
  String deskripsiPertemuan;
  DateTime _dateReg;
  JadwalMapelController jadwalMapelController =
      Get.find<JadwalMapelController>();
  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void initState() {
    _dateReg = DateTime.now();
    jadwalMapelController.getPertemuanMapel(token, widget.pelajaranId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black45, opacity: 1.0),
        title: Text(widget.namaMapel.toUpperCase(),
            style: GoogleFonts.acme(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.white)),
        //centerTitle: true,
        leading: Icon(FontAwesomeIcons.book, color: Colors.white),
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.only(bottom: 55.0),
          child: ListView.builder(
            itemCount: jadwalMapelController.pertemuanMapel.length,
            itemBuilder: (BuildContext context, int index) {
              print("cek");
              return CardPertemuan(
                pertemuan: jadwalMapelController.pertemuanMapel[index],
                namaMapel: widget.namaMapel,
                onEdit: _settingModalBottomSheet,
                pelajaranId: widget.pelajaranId,
              );
            },
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 50.0),
        child: FloatingActionButton(
          backgroundColor: Colors.green,
          child: Icon(
            Icons.add,
            size: 35,
            color: Colors.white,
          ),
          onPressed: () {
            _settingModalBottomSheet(context);
          },
        ),
      ),
    );
  }

  _settingModalBottomSheet(context) {
    // namaPertemuan = pertemuan.namaPertemuan;
    // deskripsiPertemuan = pertemuan.deskripsi;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  Container(
                    child: Text("Create Pertemuan"),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 8.0),
                    height: 50,
                    child: InkWell(
                      onTap: () async {
                        DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime.now().add(Duration(days: 360)),
                        );

                        if (picked != null && picked != _dateReg) {
                          setState(() {
                            _dateReg = picked.add(Duration(hours: 17));
                          });
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Tanggal Pertemuan",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(width: 10),
                          Row(
                            children: [
                              DateCardRow(tanggal: _dateReg),
                              SizedBox(width: 10),
                              Icon(Icons.calendar_today)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (value) => namaPertemuan = value,
                    decoration: InputDecoration(
                        labelText: 'Nama Pertemuan',
                        contentPadding: EdgeInsets.symmetric(horizontal: 16)),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (value) => deskripsiPertemuan = value,
                    maxLines: 4,
                    minLines: 4,
                    cursorColor: Colors.black54,
                    decoration: InputDecoration(
                        labelText: 'Deskripsi',
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8)),
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                      child: Text("TAMBAH",
                          style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          )),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          if (namaPertemuan != "" && deskripsiPertemuan != "") {
                            jadwalMapelController.createPertemuanMapel(
                                token,
                                widget.pelajaranId,
                                namaPertemuan,
                                deskripsiPertemuan,
                                _dateReg.toIso8601String());

                            Navigator.pop(context);
                            // showLoadingIndicator();

                            // hideLoadingIndicator();
                          } else {
                            Get.snackbar(
                              "Gagal",
                              "Semua kolom harus terisi",
                              snackPosition: SnackPosition.TOP,
                            );
                          }
                          jadwalMapelController.getPertemuanMapel(
                              token, widget.pelajaranId);
                        }
                        setState(() {});
                      },
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Colors.green[800],
                        minimumSize: Size(double.infinity, 50),
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ))
                ],
              ),
            ),
          );
        });
  }
}
