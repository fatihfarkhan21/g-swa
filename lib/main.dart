import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skuline/routes/app_routes.dart';
import 'package:skuline/views/auth_screen/first_page.dart';
import 'package:skuline/views/home/splash_screen.dart';

import 'components/app_theme.dart';
import 'components/loading.dart';
import 'controllers/app_bindings.dart';

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return Loading(
      child: GetMaterialApp(
          initialBinding: AppBindings(),  
          title: 'G-Swa',
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('id', 'ID'),
          ],
          debugShowCheckedModeBanner: false,
          theme: appTheme,
          home: SplashPage(),
          getPages: AppRoutes.routes),
    );
  }
}
