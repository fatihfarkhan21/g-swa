import 'package:get/get.dart';
import 'package:skuline/views/absensi_screen/history_presensi.dart';
import 'package:skuline/views/auth_screen/signIn_page.dart';
import 'package:skuline/views/home/app_page.dart';
import 'package:skuline/views/mapel/details_mapel.dart';
import 'package:skuline/views/mapel/mapel_page.dart';

abstract class AppRoutes {
  AppRoutes._();

  static final routes = <GetPage>[
    GetPage(name: "/home", page: ()=> AppPage()),
    GetPage(name: "/mapel", page: ()=> MapelPage()),
    GetPage(name: "/signIn", page: ()=> SignInPage()),
    GetPage(name: "/historyPresensi", page: ()=> HistoryPresensiPage()),
    GetPage(name: "/detailMapel", page: ()=> DetailsMapel()),

  ];
}