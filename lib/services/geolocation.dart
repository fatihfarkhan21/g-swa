import 'dart:math' show sin, cos, sqrt, atan2;
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:vector_math/vector_math.dart';

class GeolocationCal extends GetxController {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  var jarak = 10.0.obs;

  double earthRadius = 6371000; 

//Using pLat and pLng as dummy location
double pLat = -7.242909;   double pLng = 109.749241; 


  getUserLocation(Position _currentPosition) async {
    await getDistance(_currentPosition);
  }

//Calculating the distance between two points
  getDistance(Position _currentPosition) async {
    var dLat = radians(pLat - _currentPosition.latitude);
    var dLng = radians(pLng - _currentPosition.longitude);
    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos( radians(_currentPosition.latitude)) *
            cos(radians(pLat)) *
            sin(dLng / 2) *
            sin(dLng / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var totalDistance = earthRadius * c;

    jarak.value = totalDistance;
    var test = jarak.value;

    print(test);
    print(totalDistance); //d is the distance in meters
  }
}
