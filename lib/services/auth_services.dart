import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:skuline/models/auth_model.dart';

class AuthServices {
  static String baseUrl = "https://apisekolah.diengvalley.com/";

  const AuthServices();

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<AuthModel> login(String username, String password) async {
    Response response = await dio.post('login/proses',
        data: {"user_name": username, "password": password});
    var res = json.decode(response.data);

    AuthModel auth = AuthModel.fromMap(res);

    return auth;
  }
}
