import 'dart:convert';

import 'package:equatable/equatable.dart';

class AppModel extends Equatable {
  final bool isLoading;
  final bool isAuth;
  final String appName;
  final String deviceId;
  AppModel({
    this.isLoading = false,
    this.isAuth = false,
    this.appName = '',
    this.deviceId = '',
  });

  AppModel copyWith({
    bool isLoading,
    bool isAuth,
    String appName,
    String deviceId,
  }) {
    return AppModel(
      isLoading: isLoading ?? this.isLoading,
      isAuth: isAuth ?? this.isAuth,
      appName: appName ?? this.appName,
      deviceId: deviceId ?? this.deviceId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'isLoading': isLoading,
      'isAuth': isAuth,
      'appName': appName,
      'deviceId': deviceId,
    };
  }

  factory AppModel.fromMap(Map<String, dynamic> map) {
    return AppModel(
      isLoading: map['isLoading'],
      isAuth: map['isAuth'],
      appName: map['appName'],
      deviceId: map['deviceId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AppModel.fromJson(String source) => AppModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [isLoading, isAuth, appName, deviceId];
}
