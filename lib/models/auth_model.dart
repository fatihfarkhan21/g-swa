import 'dart:convert';

import 'package:equatable/equatable.dart';

class AuthModel extends Equatable {
  final bool pesan;
  final String token;
  AuthModel({
    this.pesan,
    this.token,
  });

  AuthModel copyWith({
    bool pesan,
    String token,
  }) {
    return AuthModel(
      pesan: pesan ?? this.pesan,
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'pesan': pesan,
      'token': token,
    };
  }

  factory AuthModel.fromMap(Map<String, dynamic> map) {
    return AuthModel(
      pesan: map['pesan'],
      token: map['token'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthModel.fromJson(String source) =>
      AuthModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [pesan, token];
}
