import 'dart:convert';

import 'package:equatable/equatable.dart';

class Materi extends Equatable {
  final String materiPertemuanId;
  final String pertemuanId;
  final String bankMateriId;
  final String keteranganMateri;
  final String bankMateriLinkId;
  final String judulLink;
  final String link;
  final String bankMateriFileId;
  final String judulFile;
  final String file;
  Materi({
    this.materiPertemuanId = '',
    this.pertemuanId = '',
    this.bankMateriId = '',
    this.keteranganMateri = '',
    this.bankMateriLinkId = '',
    this.judulLink = '',
    this.link = '',
    this.bankMateriFileId = '',
    this.judulFile = '',
    this.file = '',
  });

  Materi copyWith({
    String materiPertemuanId,
    String pertemuanId,
    String bankMateriId,
    String keteranganMateri,
    String bankMateriLinkId,
    String judulLink,
    String link,
    String bankMateriFileId,
    String judulFile,
    String file,
  }) {
    return Materi(
      materiPertemuanId: materiPertemuanId ?? this.materiPertemuanId,
      pertemuanId: pertemuanId ?? this.pertemuanId,
      bankMateriId: bankMateriId ?? this.bankMateriId,
      keteranganMateri: keteranganMateri ?? this.keteranganMateri,
      bankMateriLinkId: bankMateriLinkId ?? this.bankMateriLinkId,
      judulLink: judulLink ?? this.judulLink,
      link: link ?? this.link,
      bankMateriFileId: bankMateriFileId ?? this.bankMateriFileId,
      judulFile: judulFile ?? this.judulFile,
      file: file ?? this.file,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'materi_pertemuan_id': materiPertemuanId,
      'pertemuan_id': pertemuanId,
      'bank_materi_id': bankMateriId,
      'keterangan_materi': keteranganMateri,
      'bank_materi_link_id': bankMateriLinkId,
      'judul_link': judulLink,
      'link': link,
      'bank_materi_file_id': bankMateriFileId,
      'judul_file': judulFile,
      'file': file,
    };
  }

  factory Materi.fromMap(Map<String, dynamic> map) {
    return Materi(
      materiPertemuanId: map['materi_pertemuan_id'],
      pertemuanId: map['pertemuan_id'],
      bankMateriId: map['bank_materi_id'],
      keteranganMateri: map['keterangan_materi'],
      bankMateriLinkId: map['bank_materi_link_id'],
      judulLink: map['judul_link'],
      link: map['link'],
      bankMateriFileId: map['bank_materi_file_id'],
      judulFile: map['judul_file'],
      file: map['file'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Materi.fromJson(String source) => Materi.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      materiPertemuanId,
      pertemuanId,
      bankMateriId,
      keteranganMateri,
      bankMateriLinkId,
      judulLink,
      link,
      bankMateriFileId,
      judulFile,
      file,
    ];
  }
}
