import 'dart:convert';

import 'package:equatable/equatable.dart';

class Komen extends Equatable {
  final String komentarPertemuanId;
  final String pertemuanId;
  final String pendudukName;
  final String isiKomentar;
  final String userId;
  final String desaInfo;
  final String createdBy;
  final String createdTime;
  final String updatedBy;
  final String updatedTime;
  final String deletedBy;
  final String deletedTime;
  final String status;
  Komen({
    this.komentarPertemuanId = '',
    this.pertemuanId = '',
    this.pendudukName = '',
    this.isiKomentar = '',
    this.userId = '',
    this.desaInfo = '',
    this.createdBy = '',
    this.createdTime = '',
    this.updatedBy = '',
    this.updatedTime = '',
    this.deletedBy = '',
    this.deletedTime = '',
    this.status = '',
  });


  Komen copyWith({
    String komentarPertemuanId,
    String pertemuanId,
    String pendudukName,
    String isiKomentar,
    String userId,
    String createdBy,
    String createdTime,
    String updatedBy,
    String updatedTime,
    String deletedBy,
    String deletedTime,
    String status,
  }) {
    return Komen(
      komentarPertemuanId: komentarPertemuanId ?? this.komentarPertemuanId,
      pertemuanId: pertemuanId ?? this.pertemuanId,
      pendudukName: pendudukName ?? this.pendudukName,
      isiKomentar: isiKomentar ?? this.isiKomentar,
      userId: userId ?? this.userId,
      createdBy: createdBy ?? this.createdBy,
      createdTime: createdTime ?? this.createdTime,
      updatedBy: updatedBy ?? this.updatedBy,
      updatedTime: updatedTime ?? this.updatedTime,
      deletedBy: deletedBy ?? this.deletedBy,
      deletedTime: deletedTime ?? this.deletedTime,
      status: status ?? this.status,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'komentar_pertemuan_id': komentarPertemuanId,
      'pertemuan_id': pertemuanId,
      'penduduk_name': pendudukName,
      'isi_komentar': isiKomentar,
      'user_id': userId,
      'created_by': createdBy,
      'created_time': createdTime,
      'updated_by': updatedBy,
      'updated_time': updatedTime,
      'deleted_by': deletedBy,
      'deleted_time': deletedTime,
      'status': status,
    };
  }

  factory Komen.fromMap(Map<String, dynamic> map) {
    return Komen(
      komentarPertemuanId: map['komentar_pertemuan_id'],
      pertemuanId: map['pertemuan_id'],
      pendudukName: map['penduduk_name'],
      isiKomentar: map['isi_komentar'],
      userId: map['user_id'],
      createdBy: map['created_by'],
      createdTime: map['created_time'],
      updatedBy: map['updated_by'],
      updatedTime: map['updated_time'],
      deletedBy: map['deleted_by'],
      deletedTime: map['deleted_time'],
      status: map['status'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Komen.fromJson(String source) => Komen.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      komentarPertemuanId,
      pertemuanId,
      pendudukName,
      isiKomentar,
      userId,
      createdBy,
      createdTime,
      updatedBy,
      updatedTime,
      deletedBy,
      deletedTime,
      status,
    ];
  }
}
