import 'dart:convert';

class JadwalUjianAkhirModel {
  final String jadwalAkhirId;
  final String namaMapel;
  final String kodeJadwal;
  final String tanggal;
  final String jamMulai;
  final String jamSelesai;
  final String tanggalUlang;
  final String jamMulaiUlang;
  final String jamSelesaiUlang;
  final String tanggaRemidi;
  final String jamMulaiRemidi;
  final String jamSelesaiRemidi;
  final String penjelasan;
  JadwalUjianAkhirModel({
    this.jadwalAkhirId,
    this.namaMapel,
    this.kodeJadwal,
    this.tanggal,
      this.jamMulai,
      this.jamSelesai,
      this.tanggalUlang,
      this.jamMulaiUlang,
      this.jamSelesaiUlang,
      this.tanggaRemidi,
      this.jamMulaiRemidi,
      this.jamSelesaiRemidi,
      this.penjelasan,
  });

  JadwalUjianAkhirModel copyWith({
    String jadwalAkhirId,
    String namaMapel,
    String kodeJadwal,
    String tanggal,
    String jamMulai,
    String jamSelesai,
    String tanggalUlang,
    String jamMulaiUlang,
    String jamSelesaiUlang,
    String tanggaRemidi,
    String jamMulaiRemidi,
    String jamSelesaiRemidi,
    String penjelasan,
  }) {
    return JadwalUjianAkhirModel(
      jadwalAkhirId: jadwalAkhirId ?? this.jadwalAkhirId,
      namaMapel: namaMapel ?? this.namaMapel,
      kodeJadwal: kodeJadwal ?? this.kodeJadwal,
      tanggal: tanggal ?? this.tanggal,
      jamMulai: jamMulai ?? this.jamMulai,
      jamSelesai: jamSelesai ?? this.jamSelesai,
      tanggalUlang: tanggalUlang ?? this.tanggalUlang,
      jamMulaiUlang: jamMulaiUlang ?? this.jamMulaiUlang,
      jamSelesaiUlang: jamSelesaiUlang ?? this.jamSelesaiUlang,
      tanggaRemidi: tanggaRemidi ?? this.tanggaRemidi,
      jamMulaiRemidi: jamMulaiRemidi ?? this.jamMulaiRemidi,
      jamSelesaiRemidi: jamSelesaiRemidi ?? this.jamSelesaiRemidi,
      penjelasan: penjelasan ?? this.penjelasan,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jadwal_akhirsemester_id': jadwalAkhirId,
      'nama_mapel': namaMapel,
      'kode_jadwal': kodeJadwal,
      'tanggal': tanggal,
      'jam_mulai': jamMulai,
      'jam_selesai': jamSelesai,
      'tanggal_ulang': tanggalUlang,
      'jam_mulai_ulang': jamMulaiUlang,
      'jam_selesai_ulang': jamSelesaiUlang,
      'tanggal_remidi': tanggaRemidi,
      'jam_mulai_remidi': jamMulaiRemidi,
      'jam_selesai_remidi': jamSelesaiRemidi,
      'penjelasan': penjelasan,
    };
  }

  factory JadwalUjianAkhirModel.fromMap(Map<String, dynamic> map) {
    return JadwalUjianAkhirModel(
      jadwalAkhirId: map['jadwal_akhirsemester_id'],
      namaMapel: map['nama_mapel'],
      kodeJadwal: map['kode_jadwal'],
      tanggal: map['tanggal'],
      jamMulai: map['jam_mulai'],
      jamSelesai: map['jam_selesai'],
      tanggalUlang: map['tanggal_ulang'],
      jamMulaiUlang: map['jam_mulai_ulang'],
      jamSelesaiUlang: map['jam_selesai_ulang'],
      tanggaRemidi: map['tanggal_remidi'],
      jamMulaiRemidi: map['jam_mulai_remidi'],
      jamSelesaiRemidi: map['jam_selesai_remidi'],
      penjelasan: map['penjelasan'],
    );
  }

  String toJson() => json.encode(toMap());

  factory JadwalUjianAkhirModel.fromJson(String source) =>
      JadwalUjianAkhirModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'JadwalUjianAkhirModel(jadwalAkhirId: $jadwalAkhirId, namaMapel: $namaMapel, kodeJadwal: $kodeJadwal, tanggal: $tanggal, jamMulai: $jamMulai, jamSelesai: $jamSelesai, tanggalUlang: $tanggalUlang, jamMulaiUlang: $jamMulaiUlang, jamSelesaiUlang: $jamSelesaiUlang, tanggaRemidi: $tanggaRemidi, jamMulaiRemidi: $jamMulaiRemidi, jamSelesaiRemidi: $jamSelesaiRemidi, penjelasan: $penjelasan)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is JadwalUjianAkhirModel &&
      other.jadwalAkhirId == jadwalAkhirId &&
      other.namaMapel == namaMapel &&
      other.kodeJadwal == kodeJadwal &&
      other.tanggal == tanggal &&
      other.jamMulai == jamMulai &&
      other.jamSelesai == jamSelesai &&
      other.tanggalUlang == tanggalUlang &&
      other.jamMulaiUlang == jamMulaiUlang &&
      other.jamSelesaiUlang == jamSelesaiUlang &&
      other.tanggaRemidi == tanggaRemidi &&
      other.jamMulaiRemidi == jamMulaiRemidi &&
      other.jamSelesaiRemidi == jamSelesaiRemidi &&
      other.penjelasan == penjelasan;
  }

  @override
  int get hashCode {
    return jadwalAkhirId.hashCode ^
      namaMapel.hashCode ^
      kodeJadwal.hashCode ^
      tanggal.hashCode ^
      jamMulai.hashCode ^
      jamSelesai.hashCode ^
      tanggalUlang.hashCode ^
      jamMulaiUlang.hashCode ^
      jamSelesaiUlang.hashCode ^
      tanggaRemidi.hashCode ^
      jamMulaiRemidi.hashCode ^
      jamSelesaiRemidi.hashCode ^
      penjelasan.hashCode;
  }
}
