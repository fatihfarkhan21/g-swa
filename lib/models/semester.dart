import 'dart:convert';

import 'package:equatable/equatable.dart';

class Semester extends Equatable {
  final String semesterId;
  final String semesterName;
  Semester({
    this.semesterId,
    this.semesterName,
  });

  Semester copyWith({
    String semesterId,
    String semesterName,
  }) {
    return Semester(
      semesterId: semesterId ?? this.semesterId,
      semesterName: semesterName ?? this.semesterName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'semester_id': semesterId,
      'semester_name': semesterName,
    };
  }

  factory Semester.fromMap(Map<String, dynamic> map) {
    return Semester(
      semesterId: map['semester_id'],
      semesterName: map['semester_name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Semester.fromJson(String source) =>
      Semester.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [semesterId, semesterName];
}
