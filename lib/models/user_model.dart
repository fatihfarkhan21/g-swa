import 'dart:convert';

import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  final String pendudukId;
  final String userId;
  final String nik;
  final String pendudukName;
  final String tempatLahir;
  final String telp;
  final String email;
  final String tanggalLahir;
  final String alamat;
  final String villageId;
  final String userName;
  final String pendudukGuruId;
  final String nip;
  final String nuptk;
  final String jenisKelaminId;
  final String agamaId;
  final String statusNikahId;
  final String jabatanId;
  final String pangkatId;
  final String golRuangId;
  final String tmtPengangakatan;
  final String noSkPengangkatan;
  final String kepegawaian;
  final String s1Jurusan;
  final String s2Jurusan;
  final String masterMapelId;
  final String jenisKelaminName;
  final String agamaName;
  final String statusNikahName;
  final String jabatanName;
  final String pangkatName;
  final String golRuangName;
  final String kepegawaianName;
  final String villageName;
  final String subRegency;
  final String subRegencyId;
  final String regencyName;
  final String regencyId;
  final String provinceId;
  final String provinsiName;
  UserModel({
    this.pendudukId = '',
    this.userId = '',
    this.nik = '',
    this.pendudukName = '',
    this.tempatLahir = '',
    this.telp = '',
    this.email = '',
    this.tanggalLahir = '',
    this.alamat = '',
    this.villageId = '',
    this.userName = '',
    this.pendudukGuruId = '',
    this.nip = '',
    this.nuptk = '',
    this.jenisKelaminId = '',
    this.agamaId = '',
    this.statusNikahId = '',
    this.jabatanId = '',
    this.pangkatId = '',
    this.golRuangId = '',
    this.tmtPengangakatan = '',
    this.noSkPengangkatan = '',
    this.kepegawaian = '',
    this.s1Jurusan = '',
    this.s2Jurusan = '',
    this.masterMapelId = '',
    this.jenisKelaminName = '',
    this.agamaName = '',
    this.statusNikahName = '',
    this.jabatanName = '',
    this.pangkatName = '',
    this.golRuangName = '',
    this.kepegawaianName = '',
    this.villageName = '',
    this.subRegency = '',
    this.subRegencyId = '',
    this.regencyName = '',
    this.regencyId = '',
    this.provinceId = '',
    this.provinsiName = '',
  });

  UserModel copyWith({
    String pendudukId,
    String userId,
    String nik,
    String pendudukName,
    String tempatLahir,
    String telp,
    String email,
    String tanggalLahir,
    String alamat,
    String villageId,
    String userName,
    String pendudukGuruId,
    String nip,
    String nuptk,
    String jenisKelaminId,
    String agamaId,
    String statusNikahId,
    String jabatanId,
    String pangkatId,
    String golRuangId,
    String tmtPengangakatan,
    String noSkPengangkatan,
    String kepegawaian,
    String s1Jurusan,
    String s2Jurusan,
    String masterMapelId,
    String jenisKelaminName,
    String agamaName,
    String statusNikahName,
    String jabatanName,
    String pangkatName,
    String golRuangName,
    String kepegawaianName,
    String villageName,
    String subRegency,
    String subRegencyId,
    String regencyName,
    String regencyId,
    String provinceId,
    String provinsiName,
  }) {
    return UserModel(
      pendudukId: pendudukId ?? this.pendudukId,
      userId: userId ?? this.userId,
      nik: nik ?? this.nik,
      pendudukName: pendudukName ?? this.pendudukName,
      tempatLahir: tempatLahir ?? this.tempatLahir,
      telp: telp ?? this.telp,
      email: email ?? this.email,
      tanggalLahir: tanggalLahir ?? this.tanggalLahir,
      alamat: alamat ?? this.alamat,
      villageId: villageId ?? this.villageId,
      userName: userName ?? this.userName,
      pendudukGuruId: pendudukGuruId ?? this.pendudukGuruId,
      nip: nip ?? this.nip,
      nuptk: nuptk ?? this.nuptk,
      jenisKelaminId: jenisKelaminId ?? this.jenisKelaminId,
      agamaId: agamaId ?? this.agamaId,
      statusNikahId: statusNikahId ?? this.statusNikahId,
      jabatanId: jabatanId ?? this.jabatanId,
      pangkatId: pangkatId ?? this.pangkatId,
      golRuangId: golRuangId ?? this.golRuangId,
      tmtPengangakatan: tmtPengangakatan ?? this.tmtPengangakatan,
      noSkPengangkatan: noSkPengangkatan ?? this.noSkPengangkatan,
      kepegawaian: kepegawaian ?? this.kepegawaian,
      s1Jurusan: s1Jurusan ?? this.s1Jurusan,
      s2Jurusan: s2Jurusan ?? this.s2Jurusan,
      masterMapelId: masterMapelId ?? this.masterMapelId,
      jenisKelaminName: jenisKelaminName ?? this.jenisKelaminName,
      agamaName: agamaName ?? this.agamaName,
      statusNikahName: statusNikahName ?? this.statusNikahName,
      jabatanName: jabatanName ?? this.jabatanName,
      pangkatName: pangkatName ?? this.pangkatName,
      golRuangName: golRuangName ?? this.golRuangName,
      kepegawaianName: kepegawaianName ?? this.kepegawaianName,
      villageName: villageName ?? this.villageName,
      subRegency: subRegency ?? this.subRegency,
      subRegencyId: subRegencyId ?? this.subRegencyId,
      regencyName: regencyName ?? this.regencyName,
      regencyId: regencyId ?? this.regencyId,
      provinceId: provinceId ?? this.provinceId,
      provinsiName: provinsiName ?? this.provinsiName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'penduduk_id': pendudukId,
      'user_id': userId,
      'nik': nik,
      'penduduk_name': pendudukName,
      'tempat_lahir': tempatLahir,
      'telp': telp,
      'email': email,
      'tanggal_lahir': tanggalLahir,
      'alamat': alamat,
      'village_id': villageId,
      'user_name': userName,
      'penduduk_guru_id': pendudukGuruId,
      'nip': nip,
      'nuptk': nuptk,
      'jenis_kelamin_id': jenisKelaminId,
      'agama_id': agamaId,
      'status_nikah_id': statusNikahId,
      'jabatan_id': jabatanId,
      'pangkat_id': pangkatId,
      'gol_ruang_id': golRuangId,
      'tmt_pengangkatan': tmtPengangakatan,
      'no_sk_pengangkatan': noSkPengangkatan,
      'kepegawaian_id': kepegawaian,
      's1_jurusan': s1Jurusan,
      's2_jurusan': s2Jurusan,
      'master_mapel_id': masterMapelId,
      'jenis_kelamin_name': jenisKelaminName,
      'agama_name': agamaName,
      'status_nikah_name': statusNikahName,
      'jabatan_name': jabatanName,
      'pangkat_name': pangkatName,
      'gol_ruang_name': golRuangName,
      'kepegawaian_name': kepegawaianName,
      'village_name': villageName,
      'sub_regency_name': subRegency,
      'sub_regency_id': subRegencyId,
      'regency_name': regencyName,
      'regency_id': regencyId,
      'province_id': provinceId,
      'province_name': provinsiName,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      pendudukId: map['penduduk_id'],
      userId: map['user_id'],
      nik: map['nik'],
      pendudukName: map['penduduk_name'],
      tempatLahir: map['tempat_lahir'],
      telp: map['telp'],
      email: map['email'],
      tanggalLahir: map['tanggal_lahir'],
      alamat: map['alamat'],
      villageId: map['village_id'],
      userName: map['user_name'],
      pendudukGuruId: map['penduduk_guru_id'],
      nip: map['nip'],
      nuptk: map['nuptk'],
      jenisKelaminId: map['jenis_kelamin_id'],
      agamaId: map['agama_id'],
      statusNikahId: map['status_nikah_id'],
      jabatanId: map['jabatan_id'],
      pangkatId: map['pangkat_id'],
      golRuangId: map['gol_ruang_id'],
      tmtPengangakatan: map['tmt_pengangkatan'],
      noSkPengangkatan: map['no_sk_pengangkatan'],
      kepegawaian: map['kepegawaian_id'],
      s1Jurusan: map['s1_jurusan'],
      s2Jurusan: map['s2_jurusan'],
      masterMapelId: map['master_mapel_id'],
      jenisKelaminName: map['jenis_kelamin_name'],
      agamaName: map['agama_name'],
      statusNikahName: map['status_nikah_name'],
      jabatanName: map['jabatan_name'],
      pangkatName: map['pangkat_name'],
      golRuangName: map['gol_ruang_name'],
      kepegawaianName: map['kepegawaian_name'],
      villageName: map['village_name'],
      subRegency: map['sub_regency_name'],
      subRegencyId: map['sub_regency_id'],
      regencyName: map['regency_name'],
      regencyId: map['regency_id'],
      provinceId: map['province_id'],
      provinsiName: map['province_name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      pendudukId,
      userId,
      nik,
      pendudukName,
      tempatLahir,
      telp,
      email,
      tanggalLahir,
      alamat,
      villageId,
      userName,
      pendudukGuruId,
      nip,
      nuptk,
      jenisKelaminId,
      agamaId,
      statusNikahId,
      jabatanId,
      pangkatId,
      golRuangId,
      tmtPengangakatan,
      noSkPengangkatan,
      kepegawaian,
      s1Jurusan,
      s2Jurusan,
      masterMapelId,
      jenisKelaminName,
      agamaName,
      statusNikahName,
      jabatanName,
      pangkatName,
      golRuangName,
      kepegawaianName,
      villageName,
      subRegency,
      subRegencyId,
      regencyName,
      regencyId,
      provinceId,
      provinsiName,
    ];
  }
}
