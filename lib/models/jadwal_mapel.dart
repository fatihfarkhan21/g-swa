import 'dart:convert';

import 'package:equatable/equatable.dart';
class JadwalMapel extends Equatable {
  final String jadwalPelajaranId;
  final String guruMapelRombelId;
  final String kodeJadwal;
  final String tanggalMulai;
  final String tanggalSelesai;
  final String hari;
  final String jamMulai;
  final String jamSelesai;
  final String status;
  final String userId;
  final String mapelId;
  final String rombelId;
  final String rombelName;
  final String mapelName;
  JadwalMapel({
    this.jadwalPelajaranId = '',
    this.guruMapelRombelId = '',
    this.kodeJadwal = '',
    this.tanggalMulai = '',
    this.tanggalSelesai = '',
    this.hari = '',
    this.jamMulai = '',
    this.jamSelesai = '',
    this.status = '',
    this.userId = '',
    this.mapelId = '',
    this.rombelId = '',
    this.rombelName = '',
    this.mapelName = '',
  });

  JadwalMapel copyWith({
    String jadwalPelajaranId,
    String guruMapelRombelId,
    String kodeJadwal,
    String tanggalMulai,
    String tanggalSelesai,
    String hari,
    String jamMulai,
    String jamSelesai,
    String status,
    String userId,
    String mapelId,
    String rombelId,
    String rombelName,
    String mapelName,
  }) {
    return JadwalMapel(
      jadwalPelajaranId: jadwalPelajaranId ?? this.jadwalPelajaranId,
      guruMapelRombelId: guruMapelRombelId ?? this.guruMapelRombelId,
      kodeJadwal: kodeJadwal ?? this.kodeJadwal,
      tanggalMulai: tanggalMulai ?? this.tanggalMulai,
      tanggalSelesai: tanggalSelesai ?? this.tanggalSelesai,
      hari: hari ?? this.hari,
      jamMulai: jamMulai ?? this.jamMulai,
      jamSelesai: jamSelesai ?? this.jamSelesai,
      status: status ?? this.status,
      userId: userId ?? this.userId,
      mapelId: mapelId ?? this.mapelId,
      rombelId: rombelId ?? this.rombelId,
      rombelName: rombelName ?? this.rombelName,
      mapelName: mapelName ?? this.mapelName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jadwal_pelajaran_id': jadwalPelajaranId,
      'guru_mapel_rombel_id': guruMapelRombelId,
      'kode_jadwal': kodeJadwal,
      'tanggal_mulai': tanggalMulai,
      'tanggal_selesai': tanggalSelesai,
      'hari': hari,
      'jam_mulai': jamMulai,
      'jam_selesai': jamSelesai,
      'status': status,
      'user_id': userId,
      'mapel_id': mapelId,
      'rombel_id': rombelId,
      'rombel_name': rombelName,
      'mapel_name': mapelName,
    };
  }

  factory JadwalMapel.fromMap(Map<String, dynamic> map) {
    return JadwalMapel(
      jadwalPelajaranId: map['jadwal_pelajaran_id'],
      guruMapelRombelId: map['guru_mapel_rombel_id'],
      kodeJadwal: map['kode_jadwal'],
      tanggalMulai: map['tanggal_mulai'],
      tanggalSelesai: map['tanggal_selesai'],
      hari: map['hari'],
      jamMulai: map['jam_mulai'],
      jamSelesai: map['jam_selesai'],
      status: map['status'],
      userId: map['user_id'],
      mapelId: map['mapel_id'],
      rombelId: map['rombel_id'],
      rombelName: map['rombel_name'],
      mapelName: map['mapel_name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory JadwalMapel.fromJson(String source) => JadwalMapel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      jadwalPelajaranId,
      guruMapelRombelId,
      kodeJadwal,
      tanggalMulai,
      tanggalSelesai,
      hari,
      jamMulai,
      jamSelesai,
      status,
      userId,
      mapelId,
      rombelId,
      rombelName,
      mapelName,
    ];
  }
}
