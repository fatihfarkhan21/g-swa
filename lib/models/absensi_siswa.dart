import 'dart:convert';

import 'package:equatable/equatable.dart';

class AbsensiSiwa extends Equatable {
  final String absensiSiswaId;
  final String pertemuanId;
  final String userId;
  final String status;
  final String pendudukName;
  final String statusPresensi;
  final String tekanPresensi;
  final String waktuTekanPresensi;
  AbsensiSiwa({
    this.absensiSiswaId = '',
    this.pertemuanId = '',
    this.userId = '',
    this.status = '',
    this.pendudukName = '',
    this.statusPresensi = '',
    this.tekanPresensi = '',
    this.waktuTekanPresensi = '',
  });

  AbsensiSiwa copyWith({
    String absensiSiswaId,
    String pertemuanId,
    String userId,
    String status,
    String pendudukName,
    String statusPresensi,
    String tekanPresensi,
    String waktuTekanPresensi,
  }) {
    return AbsensiSiwa(
      absensiSiswaId: absensiSiswaId ?? this.absensiSiswaId,
      pertemuanId: pertemuanId ?? this.pertemuanId,
      userId: userId ?? this.userId,
      status: status ?? this.status,
      pendudukName: pendudukName ?? this.pendudukName,
      statusPresensi: statusPresensi ?? this.statusPresensi,
      tekanPresensi: tekanPresensi ?? this.tekanPresensi,
      waktuTekanPresensi: waktuTekanPresensi ?? this.waktuTekanPresensi,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'absensi_siswa_id': absensiSiswaId,
      'pertemuan_id': pertemuanId,
      'user_id': userId,
      'status': status,
      'penduduk_name': pendudukName,
      'status_presensi': statusPresensi,
      'tekan_presensi': tekanPresensi,
      'waktu_tekan_presensi': waktuTekanPresensi,
    };
  }

  factory AbsensiSiwa.fromMap(Map<String, dynamic> map) {
    return AbsensiSiwa(
      absensiSiswaId: map['absensi_siswa_id'],
      pertemuanId: map['pertemuan_id'],
      userId: map['user_id'],
      status: map['status'],
      pendudukName: map['penduduk_name'],
      statusPresensi: map['status_presensi'],
      tekanPresensi: map['tekan_presensi'],
      waktuTekanPresensi: map['waktu_tekan_presensi'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AbsensiSiwa.fromJson(String source) =>
      AbsensiSiwa.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      absensiSiswaId,
      pertemuanId,
      userId,
      status,
      pendudukName,
      statusPresensi,
      tekanPresensi,
      waktuTekanPresensi,
    ];
  }
}
