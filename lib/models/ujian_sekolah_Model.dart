import 'dart:convert';

import 'package:equatable/equatable.dart';

class UjianSekolah extends Equatable {
  final String ujianSekolahId;
  final String namaMapel;
  final String kodeJadwal;
  final String tanggal;
  final String jamMulai;
  final String jamSelesai;
  final String tanggalUlang;
  final String jamMulaiUlang;
  final String jamSelesaiUlang;
  final String tanggaRemidi;
  final String jamMulaiRemidi;
  final String jamSelesaiRemidi;
  final String penjelasan;
  UjianSekolah({
    this.ujianSekolahId,
    this.namaMapel,
    this.kodeJadwal,
    this.tanggal,
    this.jamMulai,
    this.jamSelesai,
    this.tanggalUlang,
    this.jamMulaiUlang,
    this.jamSelesaiUlang,
    this.tanggaRemidi,
    this.jamMulaiRemidi,
    this.jamSelesaiRemidi,
    this.penjelasan,
  });

  UjianSekolah copyWith({
    String ujianSekolahId,
    String namaMapel,
    String kodeJadwal,
    String tanggal,
    String jamMulai,
    String jamSelesai,
    String tanggalUlang,
    String jamMulaiUlang,
    String jamSelesaiUlang,
    String tanggaRemidi,
    String jamMulaiRemidi,
    String jamSelesaiRemidi,
    String penjelasan,
  }) {
    return UjianSekolah(
      ujianSekolahId: ujianSekolahId ?? this.ujianSekolahId,
      namaMapel: namaMapel ?? this.namaMapel,
      kodeJadwal: kodeJadwal ?? this.kodeJadwal,
      tanggal: tanggal ?? this.tanggal,
      jamMulai: jamMulai ?? this.jamMulai,
      jamSelesai: jamSelesai ?? this.jamSelesai,
      tanggalUlang: tanggalUlang ?? this.tanggalUlang,
      jamMulaiUlang: jamMulaiUlang ?? this.jamMulaiUlang,
      jamSelesaiUlang: jamSelesaiUlang ?? this.jamSelesaiUlang,
      tanggaRemidi: tanggaRemidi ?? this.tanggaRemidi,
      jamMulaiRemidi: jamMulaiRemidi ?? this.jamMulaiRemidi,
      jamSelesaiRemidi: jamSelesaiRemidi ?? this.jamSelesaiRemidi,
      penjelasan: penjelasan ?? this.penjelasan,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jadwal_ujiansekolah_id': ujianSekolahId,
      'nama_mapel': namaMapel,
      'kode_jadwal': kodeJadwal,
      'tanggal': tanggal,
      'jam_mulai': jamMulai,
      'jam_selesai': jamSelesai,
      'tanggal_ulang': tanggalUlang,
      'jam_mulai_ulang': jamMulaiUlang,
      'jam_selesai_ulang': jamSelesaiUlang,
      'tanggal_remidi': tanggaRemidi,
      'jam_mulai_remidi': jamMulaiRemidi,
      'jam_selesai_remidi': jamSelesaiRemidi,
      'penjelasan': penjelasan,
    };
  }

  factory UjianSekolah.fromMap(Map<String, dynamic> map) {
    return UjianSekolah(
      ujianSekolahId: map['jadwal_ujiansekolah_id'],
      namaMapel: map['nama_mapel'],
      kodeJadwal: map['kode_jadwal'],
      tanggal: map['tanggal'],
      jamMulai: map['jam_mulai'],
      jamSelesai: map['jam_selesai'],
      tanggalUlang: map['tanggal_ulang'],
      jamMulaiUlang: map['jam_mulai_ulang'],
      jamSelesaiUlang: map['jam_selesai_ulang'],
      tanggaRemidi: map['tanggal_remidi'],
      jamMulaiRemidi: map['jam_mulai_remidi'],
      jamSelesaiRemidi: map['jam_selesai_remidi'],
      penjelasan: map['penjelasan'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UjianSekolah.fromJson(String source) =>
      UjianSekolah.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      ujianSekolahId,
      namaMapel,
      kodeJadwal,
      tanggal,
      jamMulai,
      jamSelesai,
      tanggalUlang,
      jamMulaiUlang,
      jamSelesaiUlang,
      tanggaRemidi,
      jamMulaiRemidi,
      jamSelesaiRemidi,
      penjelasan,
    ];
  }
}
