import 'dart:convert';

import 'package:equatable/equatable.dart';

class SchoolModel extends Equatable {
  final String sekolahId;
  final String sekolahCode;
  final String sekolahName;
  final String sekolahInfo;
  final String telp;
  final String fax;
  final String email;
  final String alamat;
  final String villageId;
  final String latitude;
  final String longitude;
  final String villageName;
  final String subRegencyName;
  final String regencyName;
  final String provinceName;
  SchoolModel({
    this.sekolahId,
    this.sekolahCode,
    this.sekolahName,
    this.sekolahInfo,
    this.telp,
    this.fax,
    this.email,
    this.alamat,
    this.villageId,
    this.latitude,
    this.longitude,
    this.villageName,
    this.subRegencyName,
    this.regencyName,
    this.provinceName,
  });

  SchoolModel copyWith({
    String sekolahId,
    String sekolahCode,
    String sekolahName,
    String sekolahInfo,
    String telp,
    String fax,
    String email,
    String alamat,
    String villageId,
    String latitude,
    String longitude,
    String villageName,
    String subRegencyName,
    String regencyName,
    String provinceName,
  }) {
    return SchoolModel(
      sekolahId: sekolahId ?? this.sekolahId,
      sekolahCode: sekolahCode ?? this.sekolahCode,
      sekolahName: sekolahName ?? this.sekolahName,
      sekolahInfo: sekolahInfo ?? this.sekolahInfo,
      telp: telp ?? this.telp,
      fax: fax ?? this.fax,
      email: email ?? this.email,
      alamat: alamat ?? this.alamat,
      villageId: villageId ?? this.villageId,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      villageName: villageName ?? this.villageName,
      subRegencyName: subRegencyName ?? this.subRegencyName,
      regencyName: regencyName ?? this.regencyName,
      provinceName: provinceName ?? this.provinceName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'sekolah_id': sekolahId,
      'sekolah_code': sekolahCode,
      'sekolah_name': sekolahName,
      'information': sekolahInfo,
      'telp': telp,
      'fax': fax,
      'email': email,
      'alamat': alamat,
      'village_id': villageId,
      'latitude': latitude,
      'langitude': longitude,
      'village_name': villageName,
      'sub_regency_name': subRegencyName,
      'regency_name': regencyName,
      'province_name': provinceName,
    };
  }

  factory SchoolModel.fromMap(Map<String, dynamic> map) {
    return SchoolModel(
      sekolahId: map['sekolah_id'],
      sekolahCode: map['sekolah_code'],
      sekolahName: map['sekolah_name'],
      sekolahInfo: map['information'],
      telp: map['telp'],
      fax: map['fax'],
      email: map['email'],
      alamat: map['alamat'],
      villageId: map['village_id'],
      latitude: map['latitude'],
      longitude: map['langitude'],
      villageName: map['village_name'],
      subRegencyName: map['sub_regency_name'],
      regencyName: map['regency_name'],
      provinceName: map['province_name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SchoolModel.fromJson(String source) =>
      SchoolModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      sekolahId,
      sekolahCode,
      sekolahName,
      sekolahInfo,
      telp,
      fax,
      email,
      alamat,
      villageId,
      latitude,
      longitude,
      villageName,
      subRegencyName,
      regencyName,
      provinceName,
    ];
  }
}
