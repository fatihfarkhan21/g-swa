import 'dart:convert';

import 'package:equatable/equatable.dart';

class TahunAjaran extends Equatable {
  final String tahunAjaranId;
  final String tahunAjaranName;
  TahunAjaran({
    this.tahunAjaranId,
    this.tahunAjaranName,
  });

  TahunAjaran copyWith({
    String tahunAjaranId,
    String tahunAjaranName,
  }) {
    return TahunAjaran(
      tahunAjaranId: tahunAjaranId ?? this.tahunAjaranId,
      tahunAjaranName: tahunAjaranName ?? this.tahunAjaranName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'tahun_ajaran_id': tahunAjaranId,
      'tahun_ajaran_name': tahunAjaranName,
    };
  }

  factory TahunAjaran.fromMap(Map<String, dynamic> map) {
    return TahunAjaran(
      tahunAjaranId: map['tahun_ajaran_id'],
      tahunAjaranName: map['tahun_ajaran_name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TahunAjaran.fromJson(String source) =>
      TahunAjaran.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [tahunAjaranId, tahunAjaranName];
}
