import 'dart:convert';

import 'package:equatable/equatable.dart';

class PresensiModel extends Equatable {
  final String abseniGuruId;
  final String userId;
  final String latitude;
  final String longitude;
  final String absenType;
  final String waktu;
  final String fileName;
  PresensiModel({
    this.abseniGuruId,
    this.userId,
    this.latitude,
    this.longitude,
    this.absenType,
    this.waktu,
    this.fileName
  });

  PresensiModel copyWith({
    String abseniGuruId,
    String userId,
    String latitude,
    String longitude,
    String absenType,
    String waktu,
    String fileName
  }) {
    return PresensiModel(
      abseniGuruId: abseniGuruId ?? this.abseniGuruId,
      userId: userId ?? this.userId,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      absenType: absenType ?? this.absenType,
      waktu: waktu ?? this.waktu,
      fileName: fileName
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'absensi_guru_id': abseniGuruId,
      'user_id': userId,
      'latitude': latitude,
      'longitude': longitude,
      'absen_type': absenType,
      'waktu': waktu,
      'file_name': fileName
    };
  }

  factory PresensiModel.fromMap(Map<String, dynamic> map) {
    return PresensiModel(
      abseniGuruId: map['absensi_guru_id'],
      userId: map['user_id'],
      latitude: map['latitude'],
      longitude: map['longitude'],
      absenType: map['absen_type'],
      waktu: map['waktu'],
      fileName: map['file_name']
    );
  }

  String toJson() => json.encode(toMap());

  factory PresensiModel.fromJson(String source) =>
      PresensiModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      abseniGuruId,
      userId,
      latitude,
      longitude,
      absenType,
      waktu,
      fileName
    ];
  }
}
