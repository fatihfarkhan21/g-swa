import 'dart:convert';

import 'package:equatable/equatable.dart';

class Pertemuan extends Equatable {
  final String pertemuanId;
  final String jadwalPelajaranId;
  final String namaPertemuan;
  final String tanggalPertemuan;
  final String deskripsi;
  Pertemuan({
    this.pertemuanId = '',
    this.jadwalPelajaranId = '',
    this.namaPertemuan = '',
    this.tanggalPertemuan = '',
    this.deskripsi = '',
  });

  Pertemuan copyWith({
    String pertemuanId,
    String jadwalPelajaranId,
    String namaPertemuan,
    String tanggalPertemuan,
    String deskripsi,
  }) {
    return Pertemuan(
      pertemuanId: pertemuanId ?? this.pertemuanId,
      jadwalPelajaranId: jadwalPelajaranId ?? this.jadwalPelajaranId,
      namaPertemuan: namaPertemuan ?? this.namaPertemuan,
      tanggalPertemuan: tanggalPertemuan ?? this.tanggalPertemuan,
      deskripsi: deskripsi ?? this.deskripsi,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'pertemuan_id': pertemuanId,
      'jadwal_pelajaran_id': jadwalPelajaranId,
      'nama_pertemuan': namaPertemuan,
      'tanggal_pertemuan': tanggalPertemuan,
      'deskripsi': deskripsi,
    };
  }

  factory Pertemuan.fromMap(Map<String, dynamic> map) {
    return Pertemuan(
      pertemuanId: map['pertemuan_id'],
      jadwalPelajaranId: map['jadwal_pelajaran_id'],
      namaPertemuan: map['nama_pertemuan'],
      tanggalPertemuan: map['tanggal_pertemuan'],
      deskripsi: map['deskripsi'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Pertemuan.fromJson(String source) => Pertemuan.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      pertemuanId,
      jadwalPelajaranId,
      namaPertemuan,
      tanggalPertemuan,
      deskripsi,
    ];
  }
}
