import 'dart:convert';

import 'package:equatable/equatable.dart';

class MidSemester extends Equatable {
  final String midSemesterId;
  final String kodeJadwal;
  final String tanggal;
  final String jamMulai;
  final String jamSelesai;
  final String tanggalUlang;
  final String jamMulaiUlang;
  final String jamSelesaiUlang;
  final String jamMulaiRemidi;
  final String jamSelesaiRemidi;
  final String penjelasan;
  final String tanggalRemidi;
  
  MidSemester({
    this.midSemesterId,
    this.kodeJadwal,
    this.tanggal,
    this.jamMulai,
    this.jamSelesai,
    this.tanggalUlang,
    this.jamMulaiUlang,
    this.jamSelesaiUlang,
    this.jamMulaiRemidi,
    this.jamSelesaiRemidi,
    this.penjelasan,
    this.tanggalRemidi
  });

  MidSemester copyWith({
    String midSemesterId,
    String kodeJadwal,
    String tanggal,
    String jamMulai,
    String jamSelesai,
    String tanggalUlang,
    String jamMulaiUlang,
    String jamSelesaiUlang,
    String jamMulaiRemidi,
    String jamSelesaiRemidi,
    String penjelasan,
    String tanggalRemidi
  }) {
    return MidSemester(
      midSemesterId: midSemesterId ?? this.midSemesterId,
      kodeJadwal: kodeJadwal ?? this.kodeJadwal,
      tanggal: tanggal ?? this.tanggal,
      jamMulai: jamMulai ?? this.jamMulai,
      jamSelesai: jamSelesai ?? this.jamSelesai,
      tanggalUlang: tanggalUlang ?? this.tanggalUlang,
      jamMulaiUlang: jamMulaiUlang ?? this.jamMulaiUlang,
      jamSelesaiUlang: jamSelesaiUlang ?? this.jamSelesaiUlang,
      jamMulaiRemidi: jamMulaiRemidi ?? this.jamMulaiRemidi,
      jamSelesaiRemidi: jamSelesaiRemidi ?? this.jamSelesaiRemidi,
      penjelasan: penjelasan ?? this.penjelasan,
      tanggalRemidi: tanggalRemidi ?? this.tanggalRemidi
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jadwal_midsemester_id': midSemesterId,
      'kode_jadwal': kodeJadwal,
      'tanggal': tanggal,
      'jam_mulai': jamMulai,
      'jam_selesai': jamSelesai,
      'tanggal_ulang': tanggalUlang,
      'jam_mulai_ulang': jamMulaiUlang,
      'jam_selesai_ulang': jamSelesaiUlang,
      'jam_mulai_remidi': jamMulaiRemidi,
      'jam_selesai_remidi': jamSelesaiRemidi,
      'penjelasan': penjelasan,
      'tanggal_remidi': tanggalRemidi
    };
  }

  factory MidSemester.fromMap(Map<String, dynamic> map) {
    return MidSemester(
      midSemesterId: map['jadwal_midsemester_id'],
      kodeJadwal: map['kode_jadwal'],
      tanggal: map['tanggal'],
      jamMulai: map['jam_mulai'],
      jamSelesai: map['jam_selesai'],
      tanggalUlang: map['tanggal_ulang'],
      jamMulaiUlang: map['jam_mulai_ulang'],
      jamSelesaiUlang: map['jam_selesai_ulang'],
      jamMulaiRemidi: map['jam_mulai_remidi'],
      jamSelesaiRemidi: map['jam_selesai_remidi'],
      penjelasan: map['penjelasan'],
      tanggalRemidi: map['tanggal_remidi']
    );
  }

  String toJson() => json.encode(toMap());

  factory MidSemester.fromJson(String source) =>
      MidSemester.fromMap(json.decode(source));

  @override
  bool get stringify => true; 

  @override
  List<Object> get props {
    return [
      midSemesterId,
      kodeJadwal,
      tanggal,
      jamMulai,
      jamSelesai,
      tanggalUlang,
      jamMulaiUlang,
      jamSelesaiUlang,
      jamMulaiRemidi,
      jamSelesaiRemidi,
      penjelasan,
      tanggalRemidi
    ];
  }
}
