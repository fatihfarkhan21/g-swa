import 'package:get/get.dart';
import 'package:skuline/controllers/app_controller.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/controllers.dart';
import 'package:skuline/controllers/school_controller.dart';
import 'package:skuline/controllers/user_controller.dart';

import 'jadwal_mapel.dart';

class AppBindings implements Bindings {
  @override
  void dependencies() {
    Get.put<AuthController>(AuthController(), permanent: true);
    Get.put<AppController>(AppController(), permanent:  true);
    Get.put<JadwalUjianController>(JadwalUjianController(), permanent: true);
    Get.put<SchoolController>(SchoolController(), permanent: true);
    Get.put<JadwalMapelController>(JadwalMapelController(), permanent: true);
    Get.put<UserController>(UserController(), permanent: true);
  }
}
