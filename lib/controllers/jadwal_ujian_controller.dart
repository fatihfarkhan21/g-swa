import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/models/jadwal_ujian_akhir_model.dart';
import 'package:skuline/models/mid_semester.dart';
import 'package:skuline/models/models.dart';
import 'package:skuline/models/ujian_sekolah_Model.dart';

class JadwalUjianController extends GetxController {
  RxList<TahunAjaran> tahunAjaranModel = List<TahunAjaran>.empty().obs;
  RxList<JadwalUjianAkhirModel> jadwalUjianAkhirModel =
      List<JadwalUjianAkhirModel>.empty().obs;
  RxList<MidSemester> jadwalUjianMidModel = List<MidSemester>.empty().obs;
  RxList<UjianSekolah> jadwalUjianSekolahModel = List<UjianSekolah>.empty().obs;

  List<TahunAjaran> thAjaranModel = [];
  List<Semester> semester = [];

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void onInit() {
    getJadwalAkhirSmstr(token, "0");
    getJadwalMidSmstr(token, "0");
    getJadwalUjianSekolah(token);
    super.onInit();
  }

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<List<JadwalUjianAkhirModel>> getJadwalAkhirSmstr(
      String token, String semesterId) async {
    try {
      var response = await dio.post('jadwal_ujian/data',
          data: {"token": token, "semester_id": semesterId});
      List res = json.decode(response.data);

      print("cek");

      List<JadwalUjianAkhirModel> th =
          res.map((hP) => JadwalUjianAkhirModel.fromMap(hP)).toList();

      jadwalUjianAkhirModel.value = th;

      return jadwalUjianAkhirModel;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<List<MidSemester>> getJadwalMidSmstr(
      String token, String tahunAjaran) async {
    try {
      var response = await dio.post('jadwal_ujian/jadwal_midsemester',
          data: {"token": token, "semester_id": tahunAjaran});
          print("cek");
      List res = json.decode(response.data);

      print("cek");

      List<MidSemester> th = res.map((hP) => MidSemester.fromMap(hP)).toList();

      jadwalUjianMidModel.value = th;

      return jadwalUjianMidModel;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }
  Future<List<UjianSekolah>> getJadwalUjianSekolah(
      String token) async {
    try {
      var response = await dio.post('jadwal_ujian/ujian_sekolah',
          data: {"token": token});
      List res = json.decode(response.data);

      print("cek");

      List<UjianSekolah> th = res.map((hP) => UjianSekolah.fromMap(hP)).toList();

      jadwalUjianSekolahModel.value = th;

      return jadwalUjianSekolahModel;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  
}
