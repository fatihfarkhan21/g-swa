import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:skuline/components/loading.dart';
import 'package:skuline/models/absensi_siswa.dart';
import 'package:skuline/models/jadwal_mapel.dart';
import 'package:skuline/models/komen.dart';
import 'package:skuline/models/materi.dart';
import 'package:skuline/models/pertemuan_model.dart';

import 'auth_controller.dart';

class JadwalMapelController extends GetxController {
  RxList<JadwalMapel> jadwalMapelModel = List<JadwalMapel>.empty().obs;
  RxList<Pertemuan> pertemuanMapel = List<Pertemuan>.empty().obs;
  RxList<AbsensiSiwa> daftarSiswa = List<AbsensiSiwa>.empty().obs;
  RxList<Materi> daftarMateri = List<Materi>.empty().obs;
  RxList<Komen> daftarKomen = List<Komen>.empty().obs;

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void onInit() {
    getJadwalMapel(token, "2");
    getPertemuanMapel(token, "1");
    //getDaftarSiswa(token, "12");
    getDaftarMateri(token, "0");
    // getDaftarKomentar(token, "0");
    super.onInit();
  }

  @override
  void onReady() {
    // getJadwalMapel(token, "2");
    getPertemuanMapel(token, "1");
    super.onReady();
  }

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<List<JadwalMapel>> getJadwalMapel(
      String token, String semesterId) async {
    try {
      var response = await dio.post('jadwal_pelajaran/json',
          data: {"token": token, "semester_id": semesterId});
      List res = json.decode(response.data);

      print("cek");

      List<JadwalMapel> th = res.map((hP) => JadwalMapel.fromMap(hP)).toList();

      jadwalMapelModel.value = th;

      return jadwalMapelModel;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<List<Pertemuan>> getPertemuanMapel(
      String token, String jadwalPertemuanId) async {
    try {
      var response = await dio.post('pertemuan_by_guru/pertemuan',
          data: {"token": token, "jadwal_pelajaran_id": jadwalPertemuanId});
      List res = json.decode(response.data);

      print("cek");

      List<Pertemuan> th = res.map((hP) => Pertemuan.fromMap(hP)).toList();

      print("cek");

      pertemuanMapel.value = th;

      return pertemuanMapel;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<dynamic> createPertemuanMapel(String token, String mapelId,
      String namaPertemuan, String deskripsi, String tanggal) async {
    print("cek");
    showLoadingIndicator();

    try {
      var response = await dio.post('pertemuan_by_guru/create', data: {
        "token": token,
        "jadwal_pelajaran_id": mapelId,
        "nama_pertemuan": namaPertemuan,
        "deskripsi": deskripsi,
        "tanggal_pertemuan": tanggal
      });
      var res = json.decode(response.data);
      print("cek");
      if (res["pesan"] == true) {
        hideLoadingIndicator();

        Get.snackbar(
          "Berhasil",
          "Create pertemuan Succes",
          snackPosition: SnackPosition.TOP,
        );
        // Get.offAllNamed("/detailMapel");
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Gagal",
          "Gagal Create pertemuan",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }

  Future<dynamic> updatePertemuanMapel(
      String token,
      String mapelId,
      String namaPertemuan,
      String deskripsi,
      String tanggal,
      String pertemuanId) async {
    print("cek");
    showLoadingIndicator();

    try {
      var response = await dio.post('pertemuan_by_guru/update', data: {
        "token": token,
        "jadwal_pelajaran_id": mapelId,
        "nama_pertemuan": namaPertemuan,
        "deskripsi": deskripsi,
        "tanggal_pertemuan": tanggal,
        "pertemuan_id": pertemuanId
      });
      var res = json.decode(response.data);
      print("cek");
      if (res["pesan"] == true) {
        hideLoadingIndicator();

        Get.snackbar(
          "Berhasil",
          "Update pertemuan Succes",
          snackPosition: SnackPosition.TOP,
        );
        // Get.offAllNamed("/detailMapel");
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Gagal",
          "Gagal Update pertemuan",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }

  Future<List<AbsensiSiwa>> getDaftarSiswa(
      String token, String pertemuanId) async {
    try {
      var response = await dio.post('absensi_siswa_by_guru/absensi_pertemuan',
          data: {"token": token, "pertemuan_id": pertemuanId});

      print("cek");
      List res = json.decode(response.data);

      print("cek");

      List<AbsensiSiwa> th = res.map((hP) => AbsensiSiwa.fromMap(hP)).toList();

      print("cek");

      daftarSiswa.value = th;

      return daftarSiswa;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<dynamic> updateAbsenSiswa(
      String token, String siswaId, String statusPresensi) async {
    print("cek");
    showLoadingIndicator();

    try {
      var response = await dio.post('pertemuan_by_guru/update_absen', data: {
        "token": token,
        //ganti user id
        "absensi_siswa_id": siswaId,
        "status_presensi": statusPresensi
      });
      var res = json.decode(response.data);
      print("cek");
      if (res["pesan"] == true) {
        hideLoadingIndicator();

        Get.snackbar(
          "Berhasil",
          "Update pertemuan Succes",
          snackPosition: SnackPosition.TOP,
        );
        // Get.offAllNamed("/detailMapel");
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Gagal",
          "Gagal Update pertemuan",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }

  Future<List<Materi>> getDaftarMateri(String token, String pertemuanId) async {
    try {
      var response = await dio.post(
          'pertemuan_by_guru/bank_materi_by_pertemuan_id',
          data: {"token": token, "pertemuan_id": pertemuanId});

      print("cek");
      List res = json.decode(response.data);

      print("cek");

      List<Materi> th = res.map((hP) => Materi.fromMap(hP)).toList();

      print("cek");

      daftarMateri.value = th;

      return daftarMateri;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<List<Komen>> getDaftarKomentar(
      String token, String pertemuanId) async {
    try {
      var response = await dio.post(
          'pertemuan_by_guru/komentar_pertemuan_by_pertemuan_id',
          data: {"token": token, "pertemuan_id": pertemuanId});

      print("cek");
      List res = json.decode(response.data);

      print("cek");

      List<Komen> th = res.map((hP) => Komen.fromMap(hP)).toList();

      print("cek");

      daftarKomen.value = th;

      return daftarKomen;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<dynamic> createKomentar(String token, String pertemuanId, String isiKomentar) async {
    print("cek");
    showLoadingIndicator();

    try {
      var response = await dio
          .post('pertemuan_by_guru/kirim_komentar_pertemuan_by_pertemuan_id', data: {
        "token": token,
        "pertemuan_id": pertemuanId,
        "isi_komentar": isiKomentar
      });
      var res = json.decode(response.data);
      print("cek");
      if (res["pesan"] == true) {
        hideLoadingIndicator();

        Get.snackbar(
          "Berhasil",
          "Succes memeberikan komentar",
          snackPosition: SnackPosition.TOP,
        );
        // Get.offAllNamed("/detailMapel");
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Gagal",
          "Gagal memeberikan komentar",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }
}
