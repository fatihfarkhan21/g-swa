import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/models/school_model.dart';

class SchoolController extends GetxController {
  Rx<SchoolModel> schoolModel = SchoolModel().obs;
  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void onInit() {
    getSchool(token);
    super.onInit();
  }

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<SchoolModel> getSchool(String token) async {
    
    try {
      var response =
          await dio.post('sekolah_profile/profile', data: {"token": token});
          print("cek");
      var res = json.decode(response.data);
      SchoolModel school = SchoolModel.fromMap(res);
      print("cek");

      if (school != null) {
        schoolModel.value = school;
      } else {
        Get.snackbar(
          "Failed",
          "Failed load school data",
          snackPosition: SnackPosition.TOP,
        );
      }

      return school;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }
}
