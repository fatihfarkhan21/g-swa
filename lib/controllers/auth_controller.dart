import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skuline/components/loading.dart';
import 'package:skuline/models/auth_model.dart';

class AuthController extends GetxController {
  final box = GetStorage();
  var isLoading = true.obs;
  RxBool isAuth = false.obs;
  Rx<AuthModel> authModel = AuthModel(pesan: false, token: "").obs;
  AuthModel authUser = AuthModel(pesan: false, token: "");
  //Dio dio;

  @override
  void onInit() {
    _handleAuth();
    super.onInit();
  }

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<AuthModel> login(String username, String password) async {
    print("cek");
    showLoadingIndicator();
    try {
      var response = await dio.post('login/proses',
          data: {"user_name": username, "password": password});
      var res = json.decode(response.data);
      AuthModel auth = AuthModel.fromMap(res);

      authModel.value = auth;

      if (authModel.value.pesan == true) {
        print("cek");
        hideLoadingIndicator();
        Get.snackbar(
          "Welcome",
          "",
          snackPosition: SnackPosition.TOP,
        );
        _persistToken("user", res);
        Get.offAllNamed('/home');

        print(authModel.value.token);
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Failed",
          "username atau password salah",
          snackPosition: SnackPosition.BOTTOM,
        );
      }
      return auth;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        Get.snackbar(
          "404",
          "Internet not connect",
          snackPosition: SnackPosition.TOP,
        );
      }
      throw Exception(e.message);
    }
  }

  Future<AuthModel> logout(String token) async {
    print("cek");
    showLoadingIndicator();
    try {
      var response = await dio.post('login/logout', data: {"token": token});
      var res = json.decode(response.data);
      AuthModel auth = AuthModel.fromMap(res);

      authModel.value = auth;

      if (authModel.value.pesan == true) {
        print("cek");
        hideLoadingIndicator();
        // Get.snackbar(
        //   "Welcome",
        //   "",
        //   snackPosition: SnackPosition.TOP,
        // );
        _deleteToken();
        Get.offAllNamed('/signIn');

        print(authModel.value.token);
      } else {
        hideLoadingIndicator();
        Get.snackbar(
          "Failed",
          "username atau password salah",
          snackPosition: SnackPosition.BOTTOM,
        );
      }
      return auth;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        Get.snackbar(
          "404",
          "Internet not connect",
          snackPosition: SnackPosition.TOP,
        );
      }
      throw Exception(e.message);
    }
  }

  Future<bool> resetPassword(String email) async {
    showLoadingIndicator();
    try {
      var response =
          await dio.post('login/lupa_password', data: {"email": email});
      var res = json.decode(response.data);

      if (res["pesan"] == true) {
        hideLoadingIndicator();
        Get.offAllNamed('/signIn');
        Get.snackbar(
          "Berhasil",
          "Cek email anda di kotak masuk atau SPAM",
          snackPosition: SnackPosition.TOP,
        );

        return true;
      } else {
        return false;
      }
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        Get.snackbar(
          "404",
          "Internet not connect",
          snackPosition: SnackPosition.TOP,
        );
      }
      throw Exception(e.message);
    }
  }

  Future<void> _persistToken(String key, value) async {
    await box.write(key, json.encode(value));

    print('user: $value');
  }

  Future<void> _deleteToken() async {
    await box.remove('user');
    print('Token removed');
  }

  Future<dynamic> _getToken() async {
    print("cek");
    return json.decode(box.read("user"));
  }

  Future<bool> hasToken() async {
    final String token = box.read('user') ?? '';
    if (token != '') {
      return true;
    } else {
      return false;
    }
  }

  _handleAuth() async {
    if (await hasToken()) {
      authModel.value = AuthModel.fromMap(await _getToken());
      isAuth.value = true;
      print("cek");
    } else {
      AuthModel(pesan: false, token: "");
      isAuth.value = false;
    }
  }
}
