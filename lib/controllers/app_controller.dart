import 'package:get/get.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/models/app_model.dart';

class AppController extends GetxController {
  Rx<AppModel> app =
      AppModel(isLoading: true, appName: 'G-Swa', deviceId: '', isAuth: false)
          .obs;

  @override
  void onInit() {
    super.onInit();
    appInit();
    Get.find<AuthController>().isAuth.listen((isAuth) {
      app(app.value.copyWith(isAuth: isAuth));
    });
  }

  Future<void> appInit() async {
    // print('App Init -------');
    //Future.delayed(Duration(seconds: 5));
    ever(app, goToLandingPage);
    // app(app.value.copyWith(isLoading: false));
  }

  void goToLandingPage(AppModel appStatus) {
    print('Go To landing Page');
    final auth = appStatus.isAuth;
    if (auth != null) {
      if (auth) {
        Get.offAllNamed('/home');
      } else {
        Get.offAllNamed('/signIn');
      }
      app(app.value.copyWith(isLoading: false));
    }
  }
}
