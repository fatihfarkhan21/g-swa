import 'dart:convert';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/components/loading.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/models/presensi_model.dart';
import 'package:mime/mime.dart';

class PresensiController extends GetxController {
  RxList<PresensiModel> presensiModel = List<PresensiModel>.empty().obs;

  var token = Get.find<AuthController>().authModel.value.token;
  String _dateReg;
  String _dateSampai;

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  @override
  void onInit() {
    //print("cek");

    _dateReg = DateTime.now().toIso8601String().substring(0, 10);
    _dateSampai = DateTime.now().toIso8601String().substring(0, 10);

    historyPresensi(token, _dateReg, _dateSampai);

    super.onInit();
  }

  Future<List<PresensiModel>> historyPresensi(
      String token, String tanggalMulai, String tanggalSelesai) async {
    try {
      var response = await dio.post('absensi_guru/absensi_saya', data: {
        "token": token,
        "dari_tanggal": tanggalMulai,
        "sampai_tanggal": tanggalSelesai
      });
      List res = json.decode(response.data);

      print("cek");

      List<PresensiModel> historyPresensi =
          res.map((hP) => PresensiModel.fromMap(hP)).toList();

      presensiModel.value = historyPresensi;

      print("cek");

      return historyPresensi;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
  }

  Future<dynamic> presensiMasuk(String token, String latitude, String longitude,
      BuildContext context, String fileName) async {
    print("cek");

    try {
      var response = await dio.post('absensi_guru/masuk', data: {
        "token": token,
        "latitude": latitude,
        "longitude": longitude,
        "file_name": fileName
      });
      var res = json.decode(response.data);
      print("cek");
      if (res["pesan"] == true) {
        hideLoadingIndicator();

        Get.snackbar(
          "Berhasil",
          "Anda Berhasil Melakukan Presensi",
          snackPosition: SnackPosition.TOP,
        );
        Get.offAllNamed("/home");
      } else {
        Get.snackbar(
          "Gagal",
          "Anda Gagal Melakukan Presensi",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }

  Future<dynamic> presensiPulang(String token, String latitude,
      String longitude, BuildContext context, String fileName) async {
    // showLoadingIndicator();
    try {
      var response = await dio.post('absensi_guru/pulang', data: {
        "token": token,
        "latitude": latitude,
        "longitude": longitude,
        "file_name": fileName
      });
      var res = json.decode(response.data);

      if (res["pesan"] == true) {
        //print("cek");

        hideLoadingIndicator();
        // pushNewScreenWithRouteSettings(
        //   context,
        //   settings: RouteSettings(name: '/historyPresensi'),
        //   screen: HistoryPresensiPage(),
        //   pageTransitionAnimation: PageTransitionAnimation.slideUp,
        // );
        Get.offAllNamed("/home");
        Get.snackbar(
          "Berhasil",
          "Anda Berhasil Melakukan Presensi",
          snackPosition: SnackPosition.TOP,
        );
      } else {
        Get.snackbar(
          "Gagal",
          "Anda Gagal Melakukan Presensi",
          snackPosition: SnackPosition.TOP,
        );
      }

      return res;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }

  Future upload(File _image, String token, String latitude, String longitude,
      BuildContext context, int jenis) async {
    showLoadingIndicator();
    Uri apiUrl = Uri.parse('https://apisekolah.diengvalley.com/Upload/upload');

    final mimeTypeData =
        lookupMimeType(_image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final imageUploadRequest = http.MultipartRequest('POST', apiUrl);
    final file = await http.MultipartFile.fromPath('file', _image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    imageUploadRequest.fields['file'] = mimeTypeData[1];

    imageUploadRequest.files.add(file);

    try {
      final streamedResponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedResponse);

      print("cek");
      if (response.statusCode != 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);
      var name = responseData["file_name"];
      print("cek");
      if (jenis == 0) {
        presensiMasuk(token, latitude, longitude, context, name);
      } else {
        presensiPulang(token, latitude, longitude, context, name);
      }

      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
