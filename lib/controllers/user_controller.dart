import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:skuline/models/user_model.dart';

import 'auth_controller.dart';

class UserController extends GetxController {
  Rx<UserModel> userModel = UserModel(pendudukName: "", email: "").obs;

  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void onInit() {
    if (token != "") {
      getUser(token);
    }
    print("cek");

    super.onInit();
  }

  static String baseUrl = "https://apisekolah.diengvalley.com/";

  static BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.plain,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        }
        return false;
      });
  static Dio dio = Dio(options);

  Future<UserModel> getUser(String token) async {
    try {
      var response =
          await dio.post('user_profile/profile', data: {"token": token});
      print("cek");
      var res = json.decode(response.data);

      print("cek");

      UserModel user = UserModel.fromMap(res);

      userModel.value = user;

      // print("cek");

      return user;
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw Exception("Connection  Timeout Exception");
      }
      throw Exception(e.message);
    }
    // print("cek");
  }
}
