import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skuline/views/mapel/mapel_page.dart';

class SectionTitle extends StatelessWidget {
  final String title;
  const SectionTitle({
    Key key, this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.headline6,
          ),
          GestureDetector(
            onTap: () {
              Get.to(MapelPage());
            },
            child: Text(
              "Lihat Semua...",
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }
}