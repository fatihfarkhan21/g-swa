import 'package:flutter/material.dart';

class CourseTodayCard extends StatelessWidget {
  const CourseTodayCard({
    this.width = 100,
    this.aspectRetio = 1.02,
    this.text,
    this.image, this.jadwal,
  }) : super();

  final double width, aspectRetio;
  final String text;
  final String image;
  final String jadwal;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(16),
      width: size.width,
      decoration: BoxDecoration(
          color: Colors.green[800],
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                color: Color(0xFF979797).withOpacity(0.1),
                borderRadius: BorderRadius.circular(10)),
            margin: EdgeInsets.all(8),
            child: AspectRatio(
              aspectRatio: 1.02,
              child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    image: DecorationImage(image: AssetImage(image), fit: BoxFit.fitWidth)),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
                ),
                Text("Deskripsi Mata Pelajaran", style: TextStyle(color: Colors.white),),
                Text(
                  jadwal,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
