import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:skuline/components/app_theme.dart';
import 'package:skuline/models/jadwal_mapel.dart';

class CardCourse extends StatefulWidget {
  final JadwalMapel jadwalMapel;
  const CardCourse({Key key, this.jadwalMapel}) : super(key: key);

  @override
  _CardCourseState createState() => _CardCourseState();
}

class _CardCourseState extends State<CardCourse> {
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  
  @override
  Widget build(BuildContext context) {
  var daftarCourse = <Widget>[];
  // if(widget.jadwalMapel.jadwalPelajaranId != null){
  //  daftarCourse = widget.jadwalMapel.
  // }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ExpansionTileCard(
        key: cardA,
        baseColor: color02,
        expandedColor: color02,
        borderRadius: BorderRadius.circular(5),
        children: daftarCourse,
        title: Text("${widget.jadwalMapel.mapelName}"),
      ),
    );
  }
}
