import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:skuline/models/jadwal_mapel.dart';
import 'package:skuline/views/mapel/details_mapel.dart';

class CardMapel extends StatelessWidget {
  final JadwalMapel jadwalMapel;
  const CardMapel({
    Key key,
    this.jadwalMapel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 8,
      padding: EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            child:
                Text("${jadwalMapel.mapelName}".substring(0, 2).toUpperCase()),
            backgroundColor: Colors.green,
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 250,
                child: Text(
                  "${jadwalMapel.mapelName}",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
              Container(
                width: size.width - 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${jadwalMapel.rombelName}",
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                    OutlinedButton(
                      onPressed: () {
                        pushNewScreenWithRouteSettings(
                          context,
                          settings: RouteSettings(name: '/home'),
                          screen: DetailsMapel(
                            pelajaranId: jadwalMapel.jadwalPelajaranId,
                            namaMapel: jadwalMapel.mapelName,
                          ),
                          pageTransitionAnimation:
                              PageTransitionAnimation.slideUp,
                        );
                      },
                      style: OutlinedButton.styleFrom(
                        primary: Colors.green,
                        minimumSize: Size(35, 20),
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        side: BorderSide(color: Colors.green[800], width: 1.5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                      child: Text(
                        "Lihat",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Icon(
            Icons.more_vert,
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}
