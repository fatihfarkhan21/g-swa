import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:skuline/controllers/auth_controller.dart';
import 'package:skuline/controllers/jadwal_mapel.dart';
import 'package:skuline/models/pertemuan_model.dart';
import 'package:skuline/views/mapel/absensi_siswa_page.dart';
import 'package:skuline/views/mapel/details_pertemuan_page.dart';
import 'package:skuline/widget/date_card_row.dart';

class CardPertemuan extends StatefulWidget {
  //final int pertemuan;
  final String namaMapel;
  final String pelajaranId;
  final ValueSetter<Pertemuan> onEdit;

  final Pertemuan pertemuan;
  const CardPertemuan(
      {Key key, this.pertemuan, this.namaMapel, this.onEdit, this.pelajaranId})
      : super(key: key);

  @override
  _CardPertemuanState createState() => _CardPertemuanState();
}

class _CardPertemuanState extends State<CardPertemuan> {
  String namaPertemuan;
  String deskripsiPertemuan;
  final _formKey = GlobalKey<FormState>();
  DateTime _dateReg = DateTime.now();

  JadwalMapelController jadwalMapelController =
      Get.find<JadwalMapelController>();
  var token = Get.find<AuthController>().authModel.value.token;

  @override
  void initState() {
    
    _dateReg = DateTime.now();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("cek");
    DateTime tanggal = DateTime.parse(widget.pertemuan.tanggalPertemuan);
    return InkWell(
      onTap: () {
        Get.to(DetailsPertemuanPage(
          namaMapel: widget.namaMapel,
          pertemuan: widget.pertemuan,
        ));
      },
      child: Card(
        margin: EdgeInsets.all(8),
        elevation: 3,
        child: Stack(
          children: [
            Container(
             // height: 200,
              padding: EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        //margin: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.green),
                        child: Center(
                          child: Text(
                              widget.namaMapel.substring(0, 2).toUpperCase(),
                              style: GoogleFonts.roboto(color: Colors.white)),
                        ),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 260,
                            child: Text("${widget.pertemuan.namaPertemuan}",
                            textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.subtitle1),
                                
                          ),
                          DateCardRow(tanggal: tanggal)
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text("${widget.pertemuan.deskripsi}".toUpperCase(),
                      style: Theme.of(context).textTheme.subtitle1),

                  SizedBox(
                    height: 16,
                  ),
                  // Text("Terakhir dikumpulkan pada :  April 25",
                  //     style: TextStyle(
                  //         fontWeight: FontWeight.w400, color: Colors.black87)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                Get.to(DetailsPertemuanPage(
                                  pertemuan: widget.pertemuan,
                                  namaMapel: widget.namaMapel,
                                ));
                              },
                              icon: Icon(
                                Icons.reply_outlined,
                                color: Colors.green[800],
                              )),
                          Text("Reply")
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Get.to(AbsensiSiswaPage(
                                    namaMapel: widget.namaMapel,
                                    pertemuan: widget.pertemuan,
                                  ));
                                },
                                icon: Icon(
                                  Icons.people_alt,
                                  color: Colors.green[800],
                                )),
                            Text("Absensi")
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Positioned(
                // bottom: 0,
                right: 0,
                top: 0,
                child: IconButton(
                  onPressed: () {
                    _settingModalBottomSheet(context,
                        pertemuan: widget.pertemuan);
                  },
                  icon: Icon(Icons.more_vert),
                ))
          ],
        ),
      ),
    );
  }

  _settingModalBottomSheet(context, {Pertemuan pertemuan}) {
    namaPertemuan = pertemuan.namaPertemuan;
    deskripsiPertemuan = pertemuan.deskripsi;

    print("cek");
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  SizedBox(height: 10),
                  Container(
                    child: Text(
                      "Edit Pertemuan".toUpperCase(),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 8.0),
                    height: 50,
                    child: InkWell(
                      onTap: () async {
                        DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.now(),
                          lastDate: DateTime.now().add(Duration(days: 360)),
                        );

                        if (picked != null && picked != _dateReg) {
                          setState(() {
                            _dateReg = picked.add(Duration(hours: 17));
                          });
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Tanggal Pertemuan",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(width: 10),
                          Row(
                            children: [
                              DateCardRow(tanggal: _dateReg),
                              SizedBox(width: 10),
                              Icon(Icons.calendar_today)
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (value) => namaPertemuan = value,
                    decoration: InputDecoration(
                        labelText: 'Nama Pertemuan',
                        contentPadding: EdgeInsets.symmetric(horizontal: 16)),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                    onSaved: (value) => deskripsiPertemuan = value,
                    maxLines: 4,
                    minLines: 4,
                    cursorColor: Colors.black54,
                    decoration: InputDecoration(
                        labelText: 'Deskripsi',
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8)),
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                      child: Text("UBAH",
                          style: GoogleFonts.roboto(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          )),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          if (namaPertemuan != "" && deskripsiPertemuan != "") {
                            jadwalMapelController.updatePertemuanMapel(
                                token,
                                widget.pelajaranId,
                                namaPertemuan,
                                deskripsiPertemuan,
                                _dateReg.toIso8601String(),
                                pertemuan.pertemuanId);

                            Navigator.pop(context);
                            // showLoadingIndicator();

                            // hideLoadingIndicator();
                          } else {
                            Get.snackbar(
                              "Gagal",
                              "Semua kolom harus terisi",
                              snackPosition: SnackPosition.TOP,
                            );
                          }
                          jadwalMapelController.getPertemuanMapel(
                              token, widget.pelajaranId);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Colors.green[800],
                        minimumSize: Size(double.infinity, 50),
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                      ))
                ],
              ),
            ),
          );
        });
  }
}
