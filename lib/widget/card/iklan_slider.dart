import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class IklanSlider extends StatelessWidget {
  const IklanSlider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      //margin: EdgeInsets.symmetric(horizontal: 20.0),
      height: size.height * 0.4 - 50,

      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 10),
              blurRadius: 50,
              color: Color(0xFF0C9869).withOpacity(0.5),
            )
          ]),
      child: Carousel(
        boxFit: BoxFit.fitWidth,
        images: _buildCarouselItems(),
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        autoplay: false,
        dotSize: 5,
        dotIncreasedColor: Color(0xFFFF335C),
        dotBgColor: Colors.transparent,
        dotPosition: DotPosition.bottomLeft,
        dotVerticalPadding: 5.0,
        dotSpacing: 11,
      ),
    );
  }

  _buildCarouselItems() {
    var images = [0, 1, 2, 3].map((i) {
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.15), BlendMode.srcOver),
                image: AssetImage("assets/images/bg0${i.toString()}.jpg"))),
      );
    }).toList();
    return images;
  }
}
