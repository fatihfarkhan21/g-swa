import 'package:flutter/material.dart';
import 'package:skuline/models/mid_semester.dart';
import 'package:skuline/widget/date_card_row.dart';

class CardMid extends StatelessWidget {


  final DateTime tanggal;
  final String validasiSusulan;
  final DateTime susulan;
  final String validasi;
  final DateTime remed;
  final MidSemester midSemester;

  const CardMid({Key key, this.tanggal, this.validasiSusulan, this.susulan, this.validasi, this.remed, this.midSemester, }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.only(left: 8, right: 8, top: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "${midSemester.kodeJadwal}",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            "${midSemester.penjelasan}",
            style: Theme.of(context).textTheme.bodyText2,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Tanggal",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 3),
                      child: DateCardRow(tanggal: tanggal)),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Susulan",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  validasiSusulan == "0000-00-00"
                      ? Container(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: Text("0000-00-00"))
                      : Container(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: DateCardRow(tanggal: susulan)),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Remidi",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  validasi == "0000-00-00"
                      ? Container(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: Text("0000-00-00"))
                      : Container(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: DateCardRow(tanggal: remed)),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Mulai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamMulai,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Mulai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamMulaiUlang,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Mulai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamMulaiRemidi,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Selesai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamSelesai,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Selesai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamSelesaiUlang,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Selesai",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(midSemester.jamSelesaiRemidi,
                        style: Theme.of(context).textTheme.bodyText1),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Status",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  remed == DateTime.now() ?
                  Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.blue),
                    child: Text("Tersedia",
                        style: TextStyle(color: Colors.white, fontSize: 12)),
                  ) : Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.red),
                    child: Text("Selesai",
                        style: TextStyle(color: Colors.white, fontSize: 12))),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Status",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  susulan == DateTime.now() ?
                  Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.blue),
                    child: Text("Tersedia",
                        style: TextStyle(color: Colors.white, fontSize: 12)),
                  ) : Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.red),
                    child: Text("Selesai",
                        style: TextStyle(color: Colors.white, fontSize: 12))),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Status",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  tanggal == DateTime.now() ?
                  Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.blue),
                    child: Text("Tersedia",
                        style: TextStyle(color: Colors.white, fontSize: 12)),
                  ) : Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        color: Colors.red),
                    child: Text("Selesai",
                        style: TextStyle(color: Colors.white, fontSize: 12))),
                ],
              )
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}