import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CardMateri extends StatelessWidget {
  const CardMateri({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      elevation: 3,
      child: Container(
        height: 200,
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  FontAwesomeIcons.clipboardList,
                  size: 30,
                  color: Colors.blue,
                ),
                SizedBox(
                  width: 16,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Klasifikasi Makhluk Hidup",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                        )),
                    Text("April 19, 10.00 ",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                        ))
                  ],
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Text("Judul preview dari materi ini adalah".toUpperCase(),
                style:
                    TextStyle(fontWeight: FontWeight.w700, fontSize: 18)),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlinedButton(
                  onPressed: () {},
                  child: Text("View"),
                ),
                OutlinedButton(
                  onPressed: () {},
                  child: Text("Absen"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}