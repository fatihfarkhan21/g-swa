import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateCardTall extends StatelessWidget {
  final DateTime tanggal;

  DateCardTall({Key key, @required this.tanggal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String locale = Localizations.localeOf(context).languageCode;
    DateFormat df = DateFormat("EEEE dd MMMM yyyy", locale);
    List<String> tanggalText = df.format(tanggal).split(" ");
    final  primaryColor = Theme.of(context).primaryColor;
    return Container(
      
      decoration: BoxDecoration(color: primaryColor, borderRadius: BorderRadius.all(Radius.circular(5.0))),
      padding: EdgeInsets.all(5),
      width: 70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("${tanggalText[0]}",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, height: 1, color: Colors.white)),
          Text(
            "${tanggalText[1]}",
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, height: 1, color: Colors.white),
          ),
          Text("${tanggalText[2]}",
              style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, height: 0.6, color: Colors.white)),
          Text("${tanggalText[3]}",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, height: 1.2, color: Colors.white)),
        ],
      ),
    );
  }
}
