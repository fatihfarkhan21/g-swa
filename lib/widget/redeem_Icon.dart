import 'package:flutter/material.dart';

class RedeemIcon extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function tap;

  const RedeemIcon({Key key, this.title, this.icon, this.tap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: tap,
      constraints: BoxConstraints(minWidth: 20),
      elevation: 2.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 2, color: Colors.green[800]),
                color: Colors.transparent),
            child: Icon(
              icon,
              size: 30,
              color: Colors.green[800],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5),
            child: Text(
              title,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black54),
            ),
          )
        ],
      ),
    );
  }
}
